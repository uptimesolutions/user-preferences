User Preferences Service:-
-----------------------------

POST
http://localhost:8088/user/preferences
{
	"userId": "abc.xyz",     
	"customerAccount": "FEDEX",            
	"language": "Test language 111",            
	"timezone": "Test timezone 111",               
    "defaultUnits": "Test Unit",
	"defaultSite": "f35e2a60-67e1-11ec-a5da-8fb4174fa836"
}

Response
Status - 200 ok
{"outcome":"New User Preferences created successfully."}

PUT
http://localhost:8088/user/preferences
{
    "userId": "abc.xyz",     
    "customerAccount": "FEDEX",            
    "language": "Test language 111",            
    "timezone": "Test timezone 222",               
    "defaultUnits": "Test Unit",
    "defaultSite": "f35e2a60-67e1-11ec-a5da-8fb4174fa836"
}

Response
Status - 200 ok
{"outcome":"Updated User Preferences items successfully."}


GET
http://10.1.11.5:8085/user/preferences?userId=abc.xyz

Response
Status - 200 ok
{
    "userId": "abc.xyz",
    "userPreferences": [
        {
            "userId": "abc.xyz",
            "customerAccount": "FEDEX",
            "defaultSite": "f35e2a60-67e1-11ec-a5da-8fb4174fa836",
            "defaultUnits": "Test Unit",
            "language": "Test language 111",
            "timezone": "Test timezone 111"
        }
    ],
    "outcome": "GET worked successfully."
}

GET
http://10.1.11.5:8085/user/preferences?customerAccount=FEDEX&userId=abc.xyz

Response
Status - 200 ok

{
    "userId": "abc.xyz",
    "userPreferences": [
        {
            "userId": "abc.xyz",
            "customerAccount": "FEDEX",
            "defaultSite": "f35e2a60-67e1-11ec-a5da-8fb4174fa836",
            "defaultUnits": "Test Unit",
            "language": "Test language 111",
            "timezone": "Test timezone 111"
        }
    ],
    "outcome": "GET worked successfully."
}

DELETE
http://localhost:8088/user/preferences

{
    "userId": "abc.xyz",     
    "customerAccount": "FEDEX",            
    "language": "Test language 111",            
    "timezone": "Test timezone 222",               
    "defaultUnits": "Test Unit",
    "defaultSite": "f35e2a60-67e1-11ec-a5da-8fb4174fa836"
}

Response
Status - 200 ok
"outcome":"Deleted User Preferences items successfully."}


User Sites Service :-
---------------------------

POST
http://localhost:8088/user/sites
{
	"userId": "Test userId 111",     
	"customerAccount": "Test customerAccount 111",          
	"siteId": "f35e2a60-67e1-11ec-a5da-8fb4174fa836",              
	"siteRole": "Test siteRole 111",
	"firstName": "Sam",
	"lastName": "Spade"

}

Response
Status - 200 ok
{"outcome":"New User Sites / Site Users created successfully."}

PUT
http://localhost:8088/user/sites
{
	"userId": "Test userId 111",     
	"customerAccount": "Test customerAccount 111",          
	"siteId": "f35e2a60-67e1-11ec-a5da-8fb4174fa836",              
	"siteRole": "Updated Test siteRole 111",
	"firstName": "Sam",
	"lastName": "Spade"

}

Response
Status - 200 ok
{"outcome":"Updated User Sites / Site Users items successfully."}


GET
http://localhost:8088/user/sites?userId=Test userId 111

Response
Status - 200 ok
{
    "userId": "Test userId 111",
    "userSites": [
        {
	"userId": "Test userId 111",     
	"customerAccount": "Test customerAccount 111",          
	"siteId": "f35e2a60-67e1-11ec-a5da-8fb4174fa836",              
	"siteRole": "Updated Test siteRole 111"
}
    ],
    "outcome": "GET worked successfully."
}

DELETE
http://localhost:8088/user/sites

{
	"userId": "Test userId 111",     
	"customerAccount": "Test customerAccount 111",          
	"siteId": "f35e2a60-67e1-11ec-a5da-8fb4174fa836",              
	"siteRole": "Updated Test siteRole 111"
}

Response
Status - 200 ok
"outcome":"Deleted User Sites items successfully."}

Customers

GET
http://10.1.11.5:8085/user/customers?customerAccount=ALL

Response
{
    "customers": [
        {
            "customerAccount": "FEDEX EXPRESS",
            "customerName": "FedEx",
            "fqdn": "express-dev.corp.uptime-solutions.us:8443"
        },
        {
            "customerAccount": "UPTIME",
            "customerName": "Uptime Solutions",
            "fqdn": "worldview-dev.corp.uptime-solutions.us:8443"
        }
    ]
}