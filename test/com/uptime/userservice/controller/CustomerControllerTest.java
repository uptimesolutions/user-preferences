/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.userservice.controller;

import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author madhavi
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CustomerControllerTest {
    
    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//users.cql", true, true, "worldview_dev1"));

    static CustomerController instance;
    
    @BeforeClass
    public static void setUpClass() {
        instance = new CustomerController();
    }

    /**
     * Test of getCustomersByPK method, of class CustomerController.
     */
    @Test
    public void test1_GetCustomersByPK() throws Exception {
        System.out.println("getCustomersByName");
        String customerAccount = "11111";
        String expResult = "{\"customers\":[{\"customerAccount\":\"11111\",\"customerName\":\"FEDEX EXPRESS\",\"fqdn\":\"express-dev.corp.uptime-solutions.us:8443\"}]}";
        String result = instance.getCustomersByPK(customerAccount);
        System.out.println("result - " + result);
        assertEquals(expResult, result);

    }

    /**
     * Test of getAllCustomers method, of class CustomerController.
     */
    @Test
    public void test2_GetAllCustomers() throws Exception {
        System.out.println("getAllCustomers");
        String expResult = "{\"customers\":[{\"customerAccount\":\"22222\",\"customerName\":\"FEDEX GROUND\",\"fqdn\":\"ground-dev.corp.uptime-solutions.us:8443\"},{\"customerAccount\":\"33333\",\"customerName\":\"UPTIME SOLUTIONS\",\"fqdn\":\"worldview-dev.corp.uptime-solutions.us:8443\"},{\"customerAccount\":\"11111\",\"customerName\":\"FEDEX EXPRESS\",\"fqdn\":\"express-dev.corp.uptime-solutions.us:8443\"}]}";
        String result = instance.getAllCustomers();
        System.out.println("result - " + result);
        assertEquals(expResult, result);

    }
    
}
