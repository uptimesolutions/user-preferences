/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.userservice.controller;

import com.google.gson.JsonSyntaxException;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author aswani
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AlarmNotificationControllerTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//users.cql", true, true, "worldview_dev1"));

    static AlarmNotificationController alController;

    public AlarmNotificationControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        alController = new AlarmNotificationController();
    }

    /**
     * Test of createAlarmNotification method, of class
     * AlarmNotificationController.
     * Happy path: Passing input correctly and achieving desired output. 
     */
    @Test
    public void test11_CreateAlarmNotification() {
        System.out.println("test11_CreateAlarmNotification");
        String json1 = "{\n" +
"   \"userId\":\"sam.spade@uptime-solutions.us\",\n" +
"   \"siteId\":\"a226f4d7-5fc8-4e8b-b065-592602d62bcd\",\n" +
"   \"emailAddress\":\"aswani@gmail.com,test.rest@tes.com\",\n" +
"   \"alert\":true,\n" +
"   \"fault\":true,\n" +
"   \"createAssetIds\":[\n" +
"      \"4747c4d2-0f5b-46ea-9563-c02e26a5cabf\",\n" +
"      \"c5c8f0dd-4f0f-493a-b203-94665f689959\",\n" +
"      \"8ba255c8-11bd-4925-af1c-efc2d349349c\"\n" +
"   ]\n" +
"}";
        String json2 = "{\n"
                + "      \"userId\":\"user123\",\n"
                + "      \"siteId\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa838\",\n"
                + "      \"alert\":true,\n"
                + "      \"fault\":true\n"
                + "   }";

        String result = alController.createAlarmNotification(json1);
        System.out.println("result 11 - " + result);
        String expectedResult = "{\"outcome\":\"New User Alarm Notification created successfully.\"}";
        assertEquals(expectedResult, result);
        result = alController.createAlarmNotification(json2);
        assertEquals(expectedResult, result);
    }

    /**
     * Test of createAlarmNotification method, of class AlarmNotificationController
     * 
     * Unhappy path: Failed to create Alarm Notification if userID/siteId or assetId is null.
     */
    @Test
    public void test12_CreateAlarmNotification() {
        System.out.println("test12_CreateAlarmNotification");
        String json = "{\n"
                + "      \"siteId\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa836\",\n"
                + "      \"assetId\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa837\",\n"
                + "      \"alert\":true,\n"
                + "      \"fault\":true\n"
                + "   }";

        String expetedResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = alController.createAlarmNotification(json);
        assertEquals(expetedResult, result);
    }

    /**
     * Test of createAlarmNotification method, of class AlarmNotificationController.
     * 
     * Unhappy path: Failed to create Alarm Notification if input json is empty. 
     */
    @Test
    public void test13_CreateAlarmNotification() {
        System.out.println("test13_CreateAlarmNotification");
        String json = "";
        String expetedResult = "{\"outcome\":\"Json is invalid\"}";
        String result = alController.createAlarmNotification(json);
        assertEquals(expetedResult, result);
    }

    /**
     * Test of createAlarmNotification method, of class AlarmNotificationController.
     * Unhappy path: Sending incorrect Json.
     */
    @Test(expected = JsonSyntaxException.class)
    public void test14_CreateAlarmNotification() {
        System.out.println("test14_CreateAlarmNotification");
        String json = "{\n"
                + "      \"siteId\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa836,\n"
                + "      \"assetId\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa837\",\n"
                + "      \"alert\":true,\n"
                + "      \"fault\":true\n"
                + "   }";

        Assert.fail(alController.createAlarmNotification(json));
    }

    /**
     * Test of readAlarmNotificationByUserId method, of class
     * AlarmNotificationController.
     * Happy path: Passing all inputs correctly and achieving desired output. 
     * @throws java.lang.Exception
     */
    @Test
    public void test21_ReadAlarmNotificationByUserId() throws Exception {
        System.out.println("test21_ReadAlarmNotificationByUserId");
        String expetedResult = "{\"userId\":\"user123\",\"alarmNotifications\":[{\"userId\":\"user123\",\"siteId\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa838\",\"alert\":true,\"fault\":true}],\"outcome\":\"GET worked successfully.\"}";
        String result = alController.readAlarmNotificationByUserId("user123");
        assertEquals(expetedResult, result);
    }

    /**
     * Test of readAlarmNotificationByUserId method, of class
     * AlarmNotificationController.
     * Unhappy path: When the userId is passed as null, no data is received from the database.
     * @throws java.lang.Exception
     */
    @Test
    public void test22_ReadAlarmNotificationByUserId() throws Exception {
        System.out.println("test22_ReadAlarmNotificationByUserId");
        String expetedResult = "{\"outcome\":\"Alarm Notifications items not found.\"}";
        String result = alController.readAlarmNotificationByUserId("null");
        assertEquals(expetedResult, result);
    }

    /**
     * Test of readAlarmNotificationByUserId method, of class
     * AlarmNotificationController.
     * Unhappy path: Controller throws IllegalArgumentException when userId is passed as null. 
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test23_ReadAlarmNotificationByUserId() throws Exception {
        System.out.println("test23_ReadAlarmNotificationByUserId");
        Assert.fail(alController.readAlarmNotificationByUserId(null));
    }

    /**
     * Test of readAlarmNotificationByUserIdSiteId method, of class
     * AlarmNotificationController.
     * Happy path: Passing all inputs correctly and achieving desired output. 
     * @throws java.lang.Exception
     */
    @Test
    public void test31_ReadAlarmNotificationByUserIdSiteId() throws Exception {
        System.out.println("test31_ReadAlarmNotificationByUserIdSiteId");
        String expetedResult = "{\"userId\":\"sam.spade@uptime-solutions.us\",\"alarmNotifications\":[{\"userId\":\"sam.spade@uptime-solutions.us\",\"siteId\":\"a226f4d7-5fc8-4e8b-b065-592602d62bcd\",\"assetId\":\"4747c4d2-0f5b-46ea-9563-c02e26a5cabf\",\"emailAddress\":\"aswani@gmail.com,test.rest@tes.com\",\"alert\":true,\"fault\":true},{\"userId\":\"sam.spade@uptime-solutions.us\",\"siteId\":\"a226f4d7-5fc8-4e8b-b065-592602d62bcd\",\"assetId\":\"8ba255c8-11bd-4925-af1c-efc2d349349c\",\"emailAddress\":\"aswani@gmail.com,test.rest@tes.com\",\"alert\":true,\"fault\":true},{\"userId\":\"sam.spade@uptime-solutions.us\",\"siteId\":\"a226f4d7-5fc8-4e8b-b065-592602d62bcd\",\"assetId\":\"c5c8f0dd-4f0f-493a-b203-94665f689959\",\"emailAddress\":\"aswani@gmail.com,test.rest@tes.com\",\"alert\":true,\"fault\":true}],\"outcome\":\"GET worked successfully.\"}";
        String result = alController.readAlarmNotificationByUserIdSiteId("sam.spade@uptime-solutions.us", "a226f4d7-5fc8-4e8b-b065-592602d62bcd");
        System.out.println("test31_ReadAlarmNotificationByUserIdSiteId result - " + result);
        assertEquals(expetedResult, result);
    }

    /**
     * Test of readAlarmNotificationByUserIdSiteId method, of class
     * AlarmNotificationController.
     * Unhappy path: When the userId is passed as null, no data is received from the database.
     * @throws java.lang.Exception
     */
    @Test
    public void test32_ReadAlarmNotificationByUserIdSiteId() throws Exception {
        System.out.println("test32_ReadAlarmNotificationByUserIdSiteId");
        String expetedResult = "{\"outcome\":\"Alarm Notifications items not found.\"}";
        String result = alController.readAlarmNotificationByUserIdSiteId("null", "f35e2a60-67e1-11ec-a5da-8fb4174fa836");
        assertEquals(expetedResult, result);
    }

    /**
     * Test of readAlarmNotificationByUserIdSiteId method, of class
     * AlarmNotificationController.
     * Unhappy path: Controller throws IllegalArgumentException when userId is passed as null. 
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test33_ReadAlarmNotificationByUserIdSiteId() throws Exception {
        System.out.println("test33_ReadAlarmNotificationByUserIdSiteId");
        Assert.fail(alController.readAlarmNotificationByUserIdSiteId(null, "f35e2a60-67e1-11ec-a5da-8fb4174fa836"));
    }

    /**
     * Test of readAlarmNotificationByUserIdSiteId method, of class
     * AlarmNotificationController.
     * Unhappy path: Controller throws IllegalArgumentException when siteId is passed as "null". 
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test34_ReadAlarmNotificationByUserIdSiteId() throws Exception {
        System.out.println("test34_ReadAlarmNotificationByUserIdSiteId");
        Assert.fail(alController.readAlarmNotificationByUserIdSiteId("aswani", "null"));
    }

    /**
     * Test of readAlarmNotificationByUserIdSiteId method, of class
     * AlarmNotificationController.
     * Unhappy path: Controller throws NullPointerException when siteId is passed as null. 
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test35_ReadAlarmNotificationByUserIdSiteId() throws Exception {
        System.out.println("test3_ReadAlarmNotificationByUserIdSiteId");
        Assert.fail(alController.readAlarmNotificationByUserIdSiteId("aswani", null));
    }

    /**
     * Test of readAlarmNotificationByUserIdSiteIdAssetId method, of class
     * AlarmNotificationController.
     * Happy path: Passing all inputs correctly and achieving desired output. 
     * @throws java.lang.Exception
     */
    @Test
    public void test41_ReadAlarmNotificationByUserIdSiteIdAssetId() throws Exception {
        System.out.println("test41_ReadAlarmNotificationByUserIdSiteIdAssetId");
        String expetedResult = "{\"userId\":\"sam.spade@uptime-solutions.us\",\"alarmNotifications\":[{\"userId\":\"sam.spade@uptime-solutions.us\",\"siteId\":\"a226f4d7-5fc8-4e8b-b065-592602d62bcd\",\"assetId\":\"4747c4d2-0f5b-46ea-9563-c02e26a5cabf\",\"emailAddress\":\"aswani@gmail.com,test.rest@tes.com\",\"alert\":true,\"fault\":true}],\"outcome\":\"GET worked successfully.\"}";
        String result = alController.readAlarmNotificationByUserIdSiteIdAssetId("sam.spade@uptime-solutions.us", "a226f4d7-5fc8-4e8b-b065-592602d62bcd", "4747c4d2-0f5b-46ea-9563-c02e26a5cabf");
        System.out.println("test41_ReadAlarmNotificationByUserIdSiteIdAssetId result- " + result);
        assertEquals(expetedResult, result);
    }

    /**
     * Test of readAlarmNotificationByUserIdSiteIdAssetId method, of class
     * AlarmNotificationController.
     * Unhappy path: When the userId is passed as null, no data is received from the database.
     * @throws java.lang.Exception
     */
    @Test
    public void test42_ReadAlarmNotificationByUserIdSiteIdAssetId() throws Exception {
        System.out.println("test42_ReadAlarmNotificationByUserIdSiteIdAssetId");
        String expetedResult = "{\"outcome\":\"Alarm Notifications items not found.\"}";
        String result = alController.readAlarmNotificationByUserIdSiteIdAssetId("null", "f35e2a60-67e1-11ec-a5da-8fb4174fa836", "f35e2a60-67e1-11ec-a5da-8fb4174fa837");
        assertEquals(expetedResult, result);
    }

    /**
     * Test of readAlarmNotificationByUserIdSiteIdAssetId method, of class
     * AlarmNotificationController.
     * Unhappy path: Controller throws IllegalArgumentException when userId is passed as null. 
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test43_ReadAlarmNotificationByUserIdSiteIdAssetId() throws Exception {
        System.out.println("test43_ReadAlarmNotificationByUserIdSiteIdAssetId");
        Assert.fail(alController.readAlarmNotificationByUserIdSiteIdAssetId(null, "f35e2a60-67e1-11ec-a5da-8fb4174fa836", "f35e2a60-67e1-11ec-a5da-8fb4174fa837"));
    }

    /**
     * Test of readAlarmNotificationByUserIdSiteIdAssetId method, of class
     * AlarmNotificationController.
     * Unhappy path: Controller throws IllegalArgumentException when siteId is passed as "null". 
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test44_ReadAlarmNotificationByUserIdSiteIdAssetId() throws Exception {
        System.out.println("test44_ReadAlarmNotificationByUserIdSiteIdAssetId");
        Assert.fail(alController.readAlarmNotificationByUserIdSiteIdAssetId("aswani", "null", "f35e2a60-67e1-11ec-a5da-8fb4174fa837"));
    }

    /**
     * Test of readAlarmNotificationByUserIdSiteIdAssetId method, of class
     * AlarmNotificationController.
     * Unhappy path: Controller throws NullPointerException when siteId is passed as null. 
     * @throws java.lang.Exception
     */
    @Test(expected = NullPointerException.class)
    public void test45_ReadAlarmNotificationByUserIdSiteIdAssetId() throws Exception {
        System.out.println("test45_ReadAlarmNotificationByUserIdSiteIdAssetId");
        Assert.fail(alController.readAlarmNotificationByUserIdSiteIdAssetId("aswani", null, "f35e2a60-67e1-11ec-a5da-8fb4174fa837"));
    }

    /**
     * Test of updateAlarmNotification method, of class
     * AlarmNotificationController.
     * Happy path: Passing all inputs correctly and achieving desired output. 
     */
    @Test
    public void test51_UpdateAlarmNotification() {
        System.out.println("test51_UpdateAlarmNotification");
        String update = "{\n"
                + "      \"userId\":\"aswani\",\n"
                + "      \"siteId\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa836\",\n"
                + "      \"assetId\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa837\",\n"
                + "      \"alert\":true,\n"
                + "      \"fault\":true,\n"
                + "\"createAssetIds\":[\"f35e2a60-67e1-11ec-a5da-8fb4174fa835\"],\n"
                + "\"deleteAssetIds\":[\"f35e2a60-67e1-11ec-a5da-8fb4174fa837\"]   }";
        String expUpdate = "{\"outcome\":\"Alarm Notification updated successfully.\"}";
        String result = alController.updateAlarmNotification(update);
        assertEquals(expUpdate, result);
    }

    /**
     * Test of updateAlarmNotification method, of class AlarmNotificationController.
     * 
     * Unhappy path: Passing empty input json. 
     */
    @Test
    public void test52_UpdateAlarmNotification() {
        System.out.println("test52_UpdateAlarmNotification");
        String expUpdate = "{\"outcome\":\"Json is invalid\"}";
        String result = alController.updateAlarmNotification("");
        assertEquals(expUpdate, result);
        
    }

    /**
     * Test of updateAlarmNotification method, of class
     * AlarmNotificationController.
     * 
     * Unhappy path: Passing null in input json. 
     */
    @Test
    public void test53_UpdateAlarmNotification() {
        System.out.println("test53_UpdateAlarmNotification");
        String expUpdate = "{\"outcome\":\"Json is invalid\"}";
        String result = alController.updateAlarmNotification(null);
        assertEquals(expUpdate, result);
        
    }

    /**
     * Test of updateAlarmNotification method, of class
     * AlarmNotificationController.
     * 
     * Unhappy path: Passing empty userId in input json. 
     */
    @Test
    public void test54_UpdateAlarmNotification() {
        System.out.println("test54_UpdateAlarmNotification");
        String update = "{\n"
                + "      \"userId\":\"\",\n"
                + "      \"siteId\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa837\",\n"
                + "      \"assetId\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa837\",\n"
                + "      \"alert\":true,\n"
                + "      \"fault\":true,\n"
                + "\"createAssetIds\":[\"f35e2a60-67e1-11ec-a5da-8fb4174fa835\"],\n"
                + "\"deleteAssetIds\":[\"f35e2a60-67e1-11ec-a5da-8fb4174fa836\"]   }";
        String expUpdate = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = alController.updateAlarmNotification(update);
        assertEquals(expUpdate, result);
        
    }

    /**
     * Test of deleteAlarmNotification method, of class
     * AlarmNotificationController.
     * Happy path: Passing all inputs correctly and achieving desired output. 
     */
    @Test
    public void test61_DeleteAlarmNotification() {
        System.out.println("test61_DeleteAlarmNotification");
        String json1 = "{\n"
                + "      \"userId\":\"aswani\",\n"
                + "      \"siteId\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa836\",\n"
                + "      \"assetId\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa837\",\n"
                + "      \"alert\":true,\n"
                + "      \"fault\":true\n"
                + "   }";

        String expResult = "{\"outcome\":\"Alarm Notification deleted successfully.\"}";
        String result = alController.deleteAlarmNotification(json1);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteAlarmNotification method, of class
     * AlarmNotificationController.
     * 
     * Unhappy path: Passing empty input json. 
     */
    @Test
    public void test62_DeleteAlarmNotification() {
        System.out.println("test62_DeleteAlarmNotification");
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = alController.deleteAlarmNotification("");
        assertEquals(expResult, result);
    }
    
    /**
     * Test of deleteAlarmNotification method, of class
     * AlarmNotificationController.
     * 
     * Unhappy path: Passing null in input json. 
     */
    @Test
    public void test63_DeleteAlarmNotification() {
        System.out.println("test63_DeleteAlarmNotification");
        String expResult = "{\"outcome\":\"Json is invalid\"}";
        String result = alController.deleteAlarmNotification(null);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of deleteAlarmNotification method, of class
     * AlarmNotificationController.
     * 
     * Unhappy path: Not passing required field siteId in input json. 
     */
    @Test
    public void test64_DeleteAlarmNotification() {
        System.out.println("test64_DeleteAlarmNotification");
        String json1 = "{\n"
                + "      \"userId\":\"aswani\",\n"
                + "      \"assetId\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa837\",\n"
                + "      \"alert\":true,\n"
                + "      \"fault\":true\n"
                + "   }";

        String expResult = "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
        String result = alController.deleteAlarmNotification(json1);
        assertEquals(expResult, result);
    }
}
