/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.userservice.controller;

import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Joseph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserPreferencesControllerTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//users.cql", true, true, "worldview_dev1"));

    private static UserPreferencesController instance;
    private static StringBuilder content;

    @BeforeClass
    public static void setUpClass() {
        instance = new UserPreferencesController();
        content = new StringBuilder();
        content
                .append("{\"userId\":\"Test userId 111\",")
                .append("\"customerAccount\":\"Test customerAccount 111\",")
                .append("\"language\":\"Test language 111\",")
                .append("\"timezone\":\"Test timezone 111\",")
                .append("\"defaultUnits\":\"Test Unit\",")
                .append("\"defaultSite\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa836\"}");
    }

    /**
     * Test of createUserPreferences method, of class UserPreferencesController.
     * Happy path: Passing input correctly and achieving desired output. 
     */
    @Test
    public void test11_CreateUserPreferences() {
        System.out.println("test11_CreateUserPreferences");
        assertEquals("{\"outcome\":\"New User Preferences created successfully.\"}", instance.createUserPreferences(content.toString()));
    }

    /**
     * Test of createUserPreferences method, of class UserPreferencesController.
     * Unhappy path: Missing required field customerAccount in input json.
     */
    @Test
    public void test12_CreateUserPreferences() {
        System.out.println("test12_CreateUserPreferences");
        content = new StringBuilder();
        content
                .append("{\"userId\":\"Test userId 111\",")
                .append("\"language\":\"Test language 111\",")
                .append("\"timezone\":\"Test timezone 111\",")
                .append("\"defaultUnits\":\"Test Unit\",")
                .append("\"defaultSite\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa836\"}");

        assertEquals("{\"outcome\":\"insufficient and/or invalid data given in json\"}", instance.createUserPreferences(content.toString()));
    }

    /**
     * Test of createUserPreferences method, of class UserPreferencesController.
     * Unhappy path: Empty input json.
     */
    @Test
    public void test13_CreateUserPreferences() {
        System.out.println("test13_CreateUserPreferences");
        assertEquals("{\"outcome\":\"Json is invalid\"}", instance.createUserPreferences(""));
    }

    /**
     * Test of createUserPreferences method, of class UserPreferencesController.
     * Unhappy path: null input json.
     */
    @Test
    public void test14_CreateUserPreferences() {
        System.out.println("test14_CreateUserPreferences");
        assertEquals("{\"outcome\":\"Json is invalid\"}", instance.createUserPreferences(null));
    }

    /**
     * Test of findByPK method, of class UserPreferencesController.
     *
     * Happy path: Passing input correctly and achieving desired output. 
     * @throws java.lang.Exception
     */
    @Test
    public void test21_FindByPK() throws Exception {
        System.out.println("test21_FindByPK");
        System.out.println(" result - " + instance.findByPK("Test userId 111"));
        assertEquals("{\"userId\":\"Test userId 111\",\"userPreferences\":[{\"userId\":\"Test userId 111\",\"customerAccount\":\"Test customerAccount 111\",\"defaultSite\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa836\",\"defaultUnits\":\"Test Unit\",\"language\":\"Test language 111\",\"timezone\":\"Test timezone 111\",\"defaultSiteName\":\"Test siteName 111\"}],\"outcome\":\"GET worked successfully.\"}", instance.findByPK("Test userId 111"));
    }

    /**
     * Test of findByPK method, of class UserPreferencesController.
     *
     * Unhappy path: Controller throws IllegalArgumentException when userId is passed as empty. 
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test22_FindByPK() throws Exception {
        System.out.println("test22_FindByPK");
        fail(instance.findByPK(""));
    }

    /**
     * Test of findByPK method, of class UserPreferencesController.
     *
     * Unhappy path: Controller throws IllegalArgumentException when siteId is passed as null. 
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test23_FindByPK() throws Exception {
        System.out.println("test23_FindByPK");
        fail(instance.findByPK(null));
    }

    /**
     * Test of updateUserPreferences method, of class UserPreferencesController.
     * Happy path: Passing input correctly and achieving desired output. 
     */
    @Test
    public void test31_UpdateUserPreferences() {
        System.out.println("test31_UpdateUserPreferences");
        content = new StringBuilder();
        content
                .append("{\"userId\":\"Test userId 111\",")
                .append("\"customerAccount\":\"Test customerAccount 111\",")
                .append("\"language\":\"Test language 111\",")
                .append("\"timezone\":\"Updated Test timezone 111\",")
                .append("\"defaultUnits\":\"Test Unit\",")
                .append("\"defaultSite\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa837\"}");
        assertEquals("{\"outcome\":\"Updated User Preferences items successfully.\"}", instance.updateUserPreferences(content.toString()));
    }

    /**
     * Test of updateUserPreferences method, of class UserPreferencesController.
     * Unhappy path: Missing required field customerAccount in input json.
     */
    @Test
    public void test32_UpdateUserPreferences() {
        System.out.println("test32_UpdateUserPreferences");
        StringBuilder content = new StringBuilder();
        content
                .append("{\"userId\":\"Test userId 111\",")
                .append("\"language\":\"Test language 111\",")
                .append("\"timezone\":\"Updated Test timezone 111\",")
                .append("\"defaultUnits\":\"Test Unit\",")
                .append("\"defaultSite\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa837\"}");
        assertEquals("{\"outcome\":\"insufficient data given in json\"}", instance.updateUserPreferences(content.toString()));
    }

    /**
     * Test of updateUserPreferences method, of class UserPreferencesController.
     * Unhappy path: Empty input json.
     */
    @Test
    public void test33_UpdateUserPreferences() {
        System.out.println("test33_UpdateUserPreferences");

        assertEquals("{\"outcome\":\"Json is invalid\"}", instance.updateUserPreferences(""));
    }

    /**
     * Test of updateUserPreferences method, of class UserPreferencesController.
     * Unhappy path: null input json.
     */
    @Test
    public void test34_UpdateUserPreferences() {
        System.out.println("test34_UpdateUserPreferences");
        assertEquals("{\"outcome\":\"Json is invalid\"}", instance.updateUserPreferences(null));
    }

    /**
     * Test of deleteUserPreferences method, of class UserPreferencesController.
     * Happy path: Passing input correctly and achieving desired output. 
     */
    @Test
    public void test41_DeleteUserPreferences() {
        System.out.println("test41_DeleteUserPreferences");
        assertEquals("{\"outcome\":\"Deleted User Preferences items successfully.\"}", instance.deleteUserPreferences(content.toString()));
    }

    /**
     * Test of deleteUserPreferences method, of class UserPreferencesController.
     * Unhappy path: Missing required field customerAccount in input json.
     */
    @Test
    public void test42_DeleteUserPreferences() {
        System.out.println("test42_DeleteUserPreferences");
        StringBuilder content = new StringBuilder();
        content
                .append("{\"userId\":\"Test userId 111\",")
                .append("\"language\":\"Test language 111\",")
                .append("\"timezone\":\"Updated Test timezone 111\",")
                .append("\"defaultUnits\":\"Test Unit\",")
                .append("\"defaultSite\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa837\"}");
        assertEquals("{\"outcome\":\"Insufficient data given in json\"}", instance.deleteUserPreferences(content.toString()));
    }

    /**
     * Test of deleteUserPreferences method, of class UserPreferencesController.
     * Unhappy path: Empty input json.
     */
    @Test
    public void test43_DeleteUserPreferences() {
        System.out.println("test43_DeleteUserPreferences");
        assertEquals("{\"outcome\":\"Json is invalid\"}", instance.deleteUserPreferences(""));
    }

    /**
     * Test of deleteUserPreferences method, of class UserPreferencesController.
     * Unhappy path: null input json.
     */
    @Test
    public void test44_DeleteUserPreferences() {
        System.out.println("test44_DeleteUserPreferences");
        assertEquals("{\"outcome\":\"Json is invalid\"}", instance.deleteUserPreferences(null));
    }

}
