/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package com.uptime.userservice.controller;

import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Joseph
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserSitesControllerTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit
            = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//users.cql", true, true, "worldview_dev1"));

    private static UserSitesController instance;
    private static StringBuilder content;

    @BeforeClass
    public static void setUpClass() {
        instance = new UserSitesController();
        content = new StringBuilder();
        content
                .append("{\"userId\":\"Test userId 111\",")
                .append("\"customerAccount\":\"Test customerAccount 111\",")
                .append("\"siteId\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa836\",")
                .append("\"firstName\":\"Sam\",")
                .append("\"lastName\":\"Spade\",")
                .append("\"siteRole\":\"Test siteRole 111\"}");
    }

    /**
     * Test of createUserSites method, of class UserSitesController.
     * Happy path: Passing input correctly and achieving desired output. 
     */
    @Test
    public void test11_CreateUserSites() {
        System.out.println("test11_CreateUserSites");
        assertEquals("{\"outcome\":\"New User Sites / Site Users created successfully.\"}", instance.createUserSites(content.toString()));
    }

    /**
     * Test of createUserSites method, of class UserSitesController.
     * Unhappy path: Missing required field customerAccount in input json.
     */
    @Test
    public void test12_CreateUserSites() {
        System.out.println("test12_CreateUserSites");
        content = new StringBuilder();
        content
                .append("{\"userId\":\"Test userId 111\",")
                .append("\"siteId\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa836\",")
                .append("\"firstName\":\"Sam\",")
                .append("\"lastName\":\"Spade\",")
                .append("\"siteRole\":\"Test siteRole 111\"}");
        assertEquals("{\"outcome\":\"insufficient and/or invalid data given in json\"}", instance.createUserSites(content.toString()));
    }

    /**
     * Test of createUserSites method, of class UserSitesController.
     * Unhappy path: Empty input json.
     */
    @Test
    public void test13_CreateUserSites() {
        System.out.println("test13_CreateUserSites");
        assertEquals("{\"outcome\":\"Json is invalid\"}", instance.createUserSites(""));
    }

    /**
     * Test of createUserSites method, of class UserSitesController.
     * Unhappy path: null input json.
     */
    @Test
    public void test14_CreateUserSites() {
        System.out.println("test14_CreateUserSites");
        assertEquals("{\"outcome\":\"Json is invalid\"}", instance.createUserSites(null));
    }

    /**
     * Test of findUserSitesByPK method, of class UserSitesController.
     *
     * Happy path: Passing input correctly and achieving desired output. 
     * @throws java.lang.Exception
     */
    @Test
    public void test21_FindUserSitesByPK() throws Exception {
        System.out.println("test21_FindUserSitesByPK");
        assertEquals("{\"userId\":\"Test userId 111\",\"customerAccount\":\"Test customerAccount 111\",\"siteId\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa836\",\"userSites\":[{\"userId\":\"Test userId 111\",\"customerAccount\":\"Test customerAccount 111\",\"siteId\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa836\",\"siteRole\":\"Test siteRole 111\",\"siteName\":\"Test siteName 111\"}],\"outcome\":\"GET worked successfully.\"}", instance.findUserSitesByPK("Test userId 111", "Test customerAccount 111", "f35e2a60-67e1-11ec-a5da-8fb4174fa836"));
    }

    /**
     * Test of findUserSitesByPK method, of class UserSitesController.
     *
     * Unhappy path: When passing invalid userId, no data is received from the database.
     * @throws java.lang.Exception
     */
    @Test
    public void test22_FindUserSitesByPK() throws Exception {
        System.out.println("test22_FindUserSitesByPK");
        assertEquals("{\"outcome\":\"No User Sites items found.\"}", instance.findUserSitesByPK("Invalid User", "Test customerAccount 111", "f35e2a60-67e1-11ec-a5da-8fb4174fa836"));
    }

    /**
     * Test of findUserSitesByPK method, of class UserSitesController.
     *
     * @throws java.lang.Exception
     * Unhappy path: DAO throws IllegalArgumentException when userId is passed as "null". 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test23_FindUserSitesByPK() throws Exception {
        System.out.println("test23_FindUserSitesByPK");
        fail(instance.findUserSitesByPK(null, "Test customerAccount 111", "f35e2a60-67e1-11ec-a5da-8fb4174fa836"));
    }

    /**
     * Test of findUserSitesByPK method, of class UserSitesController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws IllegalArgumentException when passing invalid siteId. 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test24_FindUserSitesByPK() throws Exception {
        System.out.println("test24_FindUserSitesByPK");
        fail(instance.findUserSitesByPK("Test userId 111", "Test customerAccount 111", "f35e2a60-67e1-11ec-a5da"));
    }

    /**
     * Test of findUserSitesByPK method, of class UserSitesController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws NullPointerException when siteId is passed as "null". 
     */
    @Test(expected = NullPointerException.class)
    public void test25_FindUserSitesByPK() throws Exception {
        System.out.println("test25_FindUserSitesByPK");
        fail(instance.findUserSitesByPK("Test userId 111", "Test customerAccount 111", null));
    }

    /**
     * Test of findSiteUsersByPK method, of class UserSitesController.
     *
     * Happy path: Passing input correctly and achieving desired output. 
     * @throws java.lang.Exception
     */
    @Test
    public void test31_FindSiteUsersByPK() throws Exception {
        System.out.println("test31_FindSiteUsersByPK");
        assertEquals("{\"userId\":\"Test userId 111\",\"customerAccount\":\"Test customerAccount 111\",\"siteId\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa836\",\"siteUsers\":[{\"customerAccount\":\"Test customerAccount 111\",\"siteId\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa836\",\"userId\":\"Test userId 111\",\"firstName\":\"Sam\",\"lastName\":\"Spade\",\"siteRole\":\"Test siteRole 111\",\"siteName\":\"Test siteName 111\"}],\"outcome\":\"GET worked successfully.\"}", instance.findSiteUsersByPK("Test userId 111", "Test customerAccount 111", "f35e2a60-67e1-11ec-a5da-8fb4174fa836"));
    }

    /**
     * Test of findSiteUsersByPK method, of class UserSitesController.
     *
     * @throws java.lang.Exception
     * Unhappy path: When passing invalid userId, no data is received from the database.
     */
    @Test
    public void test32_FindSiteUsersByPK() throws Exception {
        System.out.println("test32_FindSiteUsersByPK");
        assertEquals("{\"outcome\":\"No User Sites items found.\"}", instance.findSiteUsersByPK("Invalid User", "Test customerAccount 111", "f35e2a60-67e1-11ec-a5da-8fb4174fa836"));
    }

    /**
     * Test of findSiteUsersByPK method, of class UserSitesController.
     *
     * @throws java.lang.Exception
     * Unhappy path: DAO throws IllegalArgumentException when userId is passed as "null". 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test33_FindSiteUsersByPK() throws Exception {
        System.out.println("test33_FindSiteUsersByPK");
        fail(instance.findSiteUsersByPK(null, "Test customerAccount 111", "f35e2a60-67e1-11ec-a5da-8fb4174fa836"));
    }

    /**
     * Test of findSiteUsersByPK method, of class UserSitesController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws IllegalArgumentException when passing invalid siteId. 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test34_FindSiteUsersByPK() throws Exception {
        System.out.println("test34_FindSiteUsersByPK");
        fail(instance.findSiteUsersByPK("Test userId 111", "Test customerAccount 111", "f35e2a60-67e1-11ec-a5da"));
    }

    /**
     * Test of findSiteUsersByPK method, of class UserSitesController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws NullPointerException when siteId is passed as "null". 
     */
    @Test(expected = NullPointerException.class)
    public void test35_FindSiteUsersByPK() throws Exception {
        System.out.println("test35_FindSiteUsersByPK");
        fail(instance.findSiteUsersByPK("Test userId 111", "Test customerAccount 111", null));
    }

    /**
     * Test of findByUserIdcustomerAccount method, of class UserSitesController.
     *
     * Happy path: Passing input correctly and achieving desired output. 
     * @throws java.lang.Exception
     */
    @Test
    public void test41_FindByUserIdcustomerAccount() throws Exception {
        System.out.println("test41_FindByUserIdcustomerAccount");
        assertEquals("{\"userId\":\"Test userId 111\",\"customerAccount\":\"Test customerAccount 111\",\"userSites\":[{\"userId\":\"Test userId 111\",\"customerAccount\":\"Test customerAccount 111\",\"siteId\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa836\",\"siteRole\":\"Test siteRole 111\",\"siteName\":\"Test siteName 111\"}],\"outcome\":\"GET worked successfully.\"}", instance.findByUserIdcustomerAccount("Test userId 111", "Test customerAccount 111"));
    }

    /**
     * Test of findByUserIdcustomerAccount method, of class UserSitesController.
     *
     * @throws java.lang.Exception
     * Unhappy path: When passing invalid userId, no data is received from the database.
     */
    @Test
    public void test42_FindByUserIdcustomerAccount() throws Exception {
        System.out.println("test42_FindByUserIdcustomerAccount");
        assertEquals("{\"outcome\":\"No User Sites items found.\"}", instance.findByUserIdcustomerAccount("Invalid User", "Test customerAccount 111"));
    }

    /**
     * Test of findByUserIdcustomerAccount method, of class UserSitesController.
     *
     * @throws java.lang.Exception
     * Unhappy path: DAO throws IllegalArgumentException when userId is passed as "null". 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test43_FindByUserIdcustomerAccount() throws Exception {
        System.out.println("test43_FindByUserIdcustomerAccount");
        fail(instance.findByUserIdcustomerAccount(null, "Test customerAccount 111"));
    }

    /**
     * Test of findBySiteIdcustomerAccount method, of class UserSitesController.
     *
     * Happy path: Passing input correctly and achieving desired output. 
     * @throws java.lang.Exception
     */
    @Test
    public void test51_FindBySiteIdcustomerAccount() throws Exception {
        System.out.println("test51_FindBySiteIdcustomerAccount");
        assertEquals("{\"siteId\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa836\",\"customerAccount\":\"Test customerAccount 111\",\"siteUsers\":[{\"customerAccount\":\"Test customerAccount 111\",\"siteId\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa836\",\"userId\":\"Test userId 111\",\"firstName\":\"Sam\",\"lastName\":\"Spade\",\"siteRole\":\"Test siteRole 111\"}],\"outcome\":\"GET worked successfully.\"}", instance.findBySiteIdcustomerAccount("f35e2a60-67e1-11ec-a5da-8fb4174fa836", "Test customerAccount 111"));
    }

    /**
     * Test of findBySiteIdcustomerAccount method, of class UserSitesController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws IllegalArgumentException when passing invalid siteId. 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test52_FindBySiteIdcustomerAccount() throws Exception {
        System.out.println("test52_FindBySiteIdcustomerAccount");
        fail(instance.findBySiteIdcustomerAccount("f35e2a60-67e1-11ec-a5da", "Test customerAccount 111"));
    }

    /**
     * Test of findBySiteIdcustomerAccount method, of class UserSitesController.
     *
     * @throws java.lang.Exception
     * Unhappy path: DAO throws IllegalArgumentException when userId is passed as "null". 
     */
    @Test(expected = NullPointerException.class)
    public void test53_FindBySiteIdcustomerAccount() throws Exception {
        System.out.println("test53_FindBySiteIdcustomerAccount");
        fail(instance.findBySiteIdcustomerAccount(null, "Test customerAccount 111"));
    }

    /**
     * Test of findBySiteIdcustomerAccount method, of class UserSitesController.
     *
     * @throws java.lang.Exception
     * Unhappy path: When passing invalid userId, no data is received from the database.
     */
    @Test
    public void test54_FindBySiteIdcustomerAccount() throws Exception {
        System.out.println("test54_FindBySiteIdcustomerAccount");
        assertEquals("{\"outcome\":\"No Sites User items found.\"}", instance.findBySiteIdcustomerAccount("f35e2a60-67e1-11ec-a5da-8fb4174fa836", "Invalid User"));
    }

    /**
     * Test of findBySiteIdcustomerAccount method, of class UserSitesController.
     *
     * @throws java.lang.Exception
     * Unhappy path: Controller throws NullPointerException when siteId is passed as "null". 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test55_FindBySiteIdcustomerAccount() throws Exception {
        System.out.println("test55_FindBySiteIdcustomerAccount");
        fail(instance.findBySiteIdcustomerAccount("f35e2a60-67e1-11ec-a5da-8fb4174fa836", null));
    }

    /**
     * Test of findByUserId method, of class UserSitesController.
     *
     * Happy path: Passing input correctly and achieving desired output. 
     * @throws java.lang.Exception
     */
    @Test
    public void test61_FindByUserId() throws Exception {
        System.out.println("test61_FindByUserId");
        assertEquals("{\"userId\":\"Test userId 111\",\"userSites\":[{\"userId\":\"Test userId 111\",\"customerAccount\":\"Test customerAccount 111\",\"siteId\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa836\",\"siteRole\":\"Test siteRole 111\",\"siteName\":\"Test siteName 111\"}],\"outcome\":\"GET worked successfully.\"}", instance.findByUserId("Test userId 111"));
    }

    /**
     * Test of findByUserId method, of class UserSitesController.
     *
     * @throws java.lang.Exception
     * Unhappy path: When passing invalid userId, no data is received from the database.
     */
    @Test
    public void test62_FindByUserId() throws Exception {
        System.out.println("test62_FindByUserId");
        assertEquals("{\"outcome\":\"No User Sites items found.\"}", instance.findByUserId("Invalid User"));
    }

    /**
     * Test of findByUserId method, of class UserSitesController.
     *
     * @throws java.lang.Exception
     * Unhappy path: DAO throws IllegalArgumentException when passing empty userId. 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test63_FindByUserId() throws Exception {
        System.out.println("test63_FindByUserId");
        fail(instance.findByUserId(""));
    }

    /**
     * Test of findByUserId method, of class UserSitesController.
     *
     * Unhappy path: DAO throws IllegalArgumentException when userId is passed as "null". 
     * @throws java.lang.Exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void test64_FindByUserId() throws Exception {
        System.out.println("test64_FindByUserId");
        fail(instance.findByUserId(null));
    }

    /**
     * Test of updateUserSites method, of class UserSitesController.
     * Happy path: Passing input correctly and achieving desired output. 
     */
    @Test
    public void test71_UpdateUserSites() {
        System.out.println("test71_UpdateUserSites");
        content = new StringBuilder();
        content
                .append("{\"userId\":\"Test userId 111\",")
                .append("\"customerAccount\":\"Test customerAccount 111\",")
                .append("\"siteId\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa836\",")
                .append("\"siteRole\":\"Updated Test siteRole 111\"}");
        assertEquals("{\"outcome\":\"Updated User Sites and Site Users items successfully.\"}", instance.updateUserSites(content.toString()));
    }

    /**
     * Test of updateUserSites method, of class UserSitesController.
     * Unhappy path: Missing required field customerAccount in input json.
     */
    @Test
    public void test72_UpdateUserSites() {
        System.out.println("test72_UpdateUserSites");
        StringBuilder content = new StringBuilder();
        content
                .append("{\"userId\":\"Test userId 111\",")
                .append("\"siteId\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa836\",")
                .append("\"siteRole\":\"Updated Test siteRole 111\"}");
        assertEquals("{\"outcome\":\"insufficient data given in json\"}", instance.updateUserSites(content.toString()));
    }

    /**
     * Test of updateUserSites method, of class UserSitesController.
     * Unhappy path: Empty input json.
     */
    @Test
    public void test73_UpdateUserSites() {
        System.out.println("test73_UpdateUserSites");
        assertEquals("{\"outcome\":\"Json is invalid\"}", instance.updateUserSites(""));
    }

    /**
     * Test of updateUserSites method, of class UserSitesController.
     * Unhappy path: null input json.
     */
    @Test
    public void test74_UpdateUserSites() {
        System.out.println("test74_UpdateUserSites");

        assertEquals("{\"outcome\":\"Json is invalid\"}", instance.updateUserSites(null));
    }

    /**
     * Test of deleteUserSites method, of class UserSitesController.
     * Happy path: Passing input correctly and achieving desired output. 
     */
    @Test
    public void test81_DeleteUserSites() {
        System.out.println("test81_DeleteUserSites");
        assertEquals("{\"outcome\":\"Deleted User Sites & Site Users items successfully.\"}", instance.deleteUserSites(content.toString()));
    }

    /**
     * Test of deleteUserSites method, of class UserSitesController.
     * Unhappy path: Missing required field customerAccount in input json.
     */
    @Test
    public void test82_DeleteUserSites() {
        System.out.println("test82_DeleteUserSites");
        StringBuilder content = new StringBuilder();
        content
                .append("{\"userId\":\"Test userId 111\",")
                .append("\"siteId\":\"f35e2a60-67e1-11ec-a5da-8fb4174fa836\",")
                .append("\"siteRole\":\"Updated Test siteRole 111\"}");
        assertEquals("{\"outcome\":\"Insufficient data given in json\"}", instance.deleteUserSites(content.toString()));
    }

    /**
     * Test of deleteUserSites method, of class UserSitesController.
     * Unhappy path: Empty input json.
     */
    @Test
    public void test83_DeleteUserSites() {
        System.out.println("test83_DeleteUserSites");
        assertEquals("{\"outcome\":\"Json is invalid\"}", instance.deleteUserSites(""));
    }

    /**
     * Test of deleteUserSites method, of class UserSitesController.
     * Unhappy path: null input json.
     */
    @Test
    public void test84_DeleteUserSites() {
        System.out.println("test84_DeleteUserSites");
        assertEquals("{\"outcome\":\"Json is invalid\"}", instance.deleteUserSites(null));
    }

}
