/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.userservice.controller;

import java.util.UUID;
import org.cassandraunit.CassandraCQLUnit;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author twilcox
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PasswordTokenControllerTest {

    @ClassRule
    public static CassandraCQLUnit cassandraCQLUnit = new CassandraCQLUnit(new ClassPathCQLDataSet("resources//users.cql", true, true, "worldview_dev1"));

    private static PasswordTokenController instance;
    private static UUID passwordToken;

    @BeforeClass
    public static void setUpClass() {
        instance = new PasswordTokenController();
        passwordToken = UUID.randomUUID();

    }

    /**
     * Test of checkByPK method, of class PasswordTokenController.
     * 
     * Happy path: Passing input correctly and achieving desired output. 
     */
    @Test
    public void test0_CheckByPK() {
        System.out.println("test0_CheckByPK");
        assertEquals("{\"found\":false,\"outcome\":\"GET worked successfully.\"}", instance.checkByPK("USER_ID", passwordToken.toString()));
    }

    /**
     * Test of createPasswordToken method, of class PasswordTokenController.
     * 
     * Happy path: Passing input correctly and achieving desired output. 
     */
    @Test
    public void test11_CreatePasswordToken() {
        System.out.println("test11_CreatePasswordToken");
        StringBuilder content = new StringBuilder();
        content.append("{\"userId\":\"USER_ID\",")
                .append("\"passwordToken\":\"").append(passwordToken).append("\"}");

        String result = instance.createPasswordToken(content.toString());
        System.out.println("test11_CreatePasswordToken result - " + result);
        assertEquals("{\"outcome\":\"New Password Token created successfully.\"}", result);
    }

    /**
     * Test of createPasswordToken method, of class PasswordTokenController.
     * 
     * Unhappy path: Controller throws IllegalArgumentException when siteId is passed as "null". 
     */
    @Test
    public void test12_CreatePasswordToken() {
        System.out.println("test12_CreatePasswordToken");
        assertEquals("{\"outcome\":\"Json is invalid\"}", instance.createPasswordToken(""));
    }

    /**
     * Test of createPasswordToken method, of class PasswordTokenController.
     * 
     * Unhappy path: Passing null for input json.
     */
    @Test
    public void test13_CreatePasswordToken() {
        System.out.println("test13_CreatePasswordToken");
        assertEquals("{\"outcome\":\"Json is invalid\"}", instance.createPasswordToken(null));
    }

    /**
     * Test of createPasswordToken method, of class PasswordTokenController.
     * Unhappy path: userId or passwordToken is empty.
     */
    @Test
    public void test14_CreatePasswordToken() {
        System.out.println("test14_CreatePasswordToken");
        StringBuilder content = new StringBuilder();
        content.append("{\"userId\":\"\",")
                .append("\"passwordToken\":\"").append(passwordToken).append("\"}");

        assertEquals("{\"outcome\":\"insufficient and/or invalid data given in json\"}", instance.createPasswordToken(content.toString()));
    }

    /**
     * Test of checkByPK method, of class PasswordTokenController.
     * 
     * Happy path: Passing input correctly and achieving desired output. 
     */
    @Test
    public void test21_CheckByPK() {
        System.out.println("test21_CheckByPK");
        assertEquals("{\"found\":true,\"outcome\":\"GET worked successfully.\"}", instance.checkByPK("USER_ID", passwordToken.toString()));
    }

    /**
     * Test of checkByPK method, of class PasswordTokenController.
     * 
     * Unhappy path: Controller throws IllegalArgumentException when passwordToken is passed as "null". 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test22_CheckByPK() {
        System.out.println("test22_CheckByPK");
        fail(instance.checkByPK("USER_ID", "null"));
    }

    /**
     * Test of checkByPK method, of class PasswordTokenController.
     * 
     * Unhappy path: Controller throws NullPointerException when passwordToken is passed as "null". 
     */
    @Test(expected = NullPointerException.class)
    public void test23_CheckByPK() {
        System.out.println("test23_CheckByPK");
        fail(instance.checkByPK("USER_ID", null));
    }

    /**
     * Test of checkByPK method, of class PasswordTokenController.
     * Unhappy path: DAO throws IllegalArgumentException when userId is passed as "null". 
     */
    @Test(expected = IllegalArgumentException.class)
    public void test24_CheckByPK() {
        System.out.println("test24_CheckByPK");
        fail(instance.checkByPK(null, passwordToken.toString()));
    }

}
