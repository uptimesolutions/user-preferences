/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.userservice.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.uptime.userservice.vo.AlarmNotificationVO;
import com.uptime.userservice.vo.PasswordTokenVO;
import com.uptime.userservice.vo.SiteUsersVO;
import com.uptime.userservice.vo.UserPreferencesVO;
import com.uptime.userservice.vo.UserSitesVO;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author Joseph
 */
public class JsonUtil {

    /**
     * Parse the given json String Object and return a UserPreferencesVO object
     *
     * @param content, String Object
     * @return UserPreferencesVO Object
     */
    public static UserPreferencesVO userPreferencesParser(String content) {
        UserPreferencesVO userPreferencesVO = null;
        JsonElement element;
        JsonObject jsonObject;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonObject = element.getAsJsonObject()) != null) {
            userPreferencesVO = new UserPreferencesVO();

            if (jsonObject.has("userId")) {
                try {
                    userPreferencesVO.setUserId(jsonObject.get("userId").getAsString());
                } catch (Exception ex) {
                }
            }
            if (jsonObject.has("customerAccount")) {
                try {
                    userPreferencesVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString());
                } catch (Exception ex) {
                }
            }
            if (jsonObject.has("language")) {
                try {
                    userPreferencesVO.setLanguage(jsonObject.get("language").getAsString());
                } catch (Exception ex) {
                }
            }
            if (jsonObject.has("timezone")) {
                try {
                    userPreferencesVO.setTimezone(jsonObject.get("timezone").getAsString());
                } catch (Exception ex) {
                }
            }
            if (jsonObject.has("defaultSite")) {
                try {
                    userPreferencesVO.setDefaultSite(UUID.fromString(jsonObject.get("defaultSite").getAsString()));
                } catch (Exception ex) {
                }
            }
            if (jsonObject.has("defaultUnits")) {
                try {
                    userPreferencesVO.setDefaultUnits(jsonObject.get("defaultUnits").getAsString());
                } catch (Exception ex) {
                }
            }
        }
        return userPreferencesVO;
    }

    /**
     * Parse the given json String Object and return a UserSitesVO object
     *
     * @param content, String Object
     * @return UserSitesVO Object
     */
    public static UserSitesVO userSitesParser(String content) {
        UserSitesVO userSitesVO = null;
        JsonElement element;
        JsonObject jsonObject;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonObject = element.getAsJsonObject()) != null) {
            userSitesVO = new UserSitesVO();

            if (jsonObject.has("userId")) {
                try {
                    userSitesVO.setUserId(jsonObject.get("userId").getAsString());
                } catch (Exception ex) {
                }
            }
            if (jsonObject.has("customerAccount")) {
                try {
                    userSitesVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString());
                } catch (Exception ex) {
                }
            }

            if (jsonObject.has("siteId")) {
                try {
                    userSitesVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString()));
                } catch (Exception ex) {
                }
            }
            if (jsonObject.has("siteRole")) {
                try {
                    userSitesVO.setSiteRole(jsonObject.get("siteRole").getAsString());
                } catch (Exception ex) {
                }
            }
        }
        return userSitesVO;
    }

    /**
     * Parse the given json String Object and return a SiteUsersVO object
     *
     * @param content, String Object
     * @return SiteUsersVO Object
     */
    public static SiteUsersVO siteUsersParser(String content) {
        SiteUsersVO siteUsersVO = null;
        JsonElement element;
        JsonObject jsonObject;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonObject = element.getAsJsonObject()) != null) {
            siteUsersVO = new SiteUsersVO();

            if (jsonObject.has("userId")) {
                try {
                    siteUsersVO.setUserId(jsonObject.get("userId").getAsString());
                } catch (Exception ex) {
                }
            }
            if (jsonObject.has("customerAccount")) {
                try {
                    siteUsersVO.setCustomerAccount(jsonObject.get("customerAccount").getAsString());
                } catch (Exception ex) {
                }
            }

            if (jsonObject.has("siteId")) {
                try {
                    siteUsersVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString()));
                } catch (Exception ex) {
                }
            }
            if (jsonObject.has("siteRole")) {
                try {
                    siteUsersVO.setSiteRole(jsonObject.get("siteRole").getAsString());
                } catch (Exception ex) {
                }
            }
            if (jsonObject.has("firstName")) {
                try {
                    siteUsersVO.setFirstName(jsonObject.get("firstName").getAsString());
                } catch (Exception ex) {
                }
            }
            if (jsonObject.has("lastName")) {
                try {
                    siteUsersVO.setLastName(jsonObject.get("lastName").getAsString());
                } catch (Exception ex) {
                }
            }
        }
        return siteUsersVO;
    }

    /**
     * Parse the given json String Object and return a List Object of
     * AlarmNotificationVO Objects
     *
     * @param content, String Object
     * @return List Object of AlarmNotificationVO Objects
     */
    public static AlarmNotificationVO alarmNotificationVOsParser(String content) {
        AlarmNotificationVO notificationVO = null;
        String assetId;
        JsonElement element;
        JsonObject jsonObject;
        JsonArray jsonArray;
        List<UUID> createAssetIds;
        List<UUID> deleteAssetIds;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null
                && (jsonObject = element.getAsJsonObject()) != null) {
            notificationVO = new AlarmNotificationVO();

            if (jsonObject.has("siteId") && !jsonObject.get("siteId").isJsonNull()) {
                try {
                    notificationVO.setSiteId(UUID.fromString(jsonObject.get("siteId").getAsString().trim()));
                } catch (Exception ex) {
                }
            }
            if (jsonObject.has("assetId") && !jsonObject.get("assetId").isJsonNull()) {
                try {
                    notificationVO.setAssetId(UUID.fromString(jsonObject.get("assetId").getAsString().trim()));
                } catch (Exception ex) {
                }
            }
            if (jsonObject.has("userId") && !jsonObject.get("userId").isJsonNull()) {
                try {
                    notificationVO.setUserId(jsonObject.get("userId").getAsString().trim());
                } catch (Exception ex) {
                }
            }
            if (jsonObject.has("emailAddress") && !jsonObject.get("emailAddress").isJsonNull()) {
                try {
                    notificationVO.setEmailAddress(jsonObject.get("emailAddress").getAsString().trim());
                } catch (Exception ex) {
                }
            }
            if (jsonObject.has("alert") && !jsonObject.get("alert").isJsonNull()) {
                try {
                    notificationVO.setAlert(jsonObject.get("alert").getAsBoolean());
                } catch (Exception ex) {
                }
            }
            if (jsonObject.has("fault") && !jsonObject.get("fault").isJsonNull()) {
                try {
                    notificationVO.setFault(jsonObject.get("fault").getAsBoolean());
                } catch (Exception ex) {
                }
            }
           

            if (jsonObject.has("createAssetIds")) {
                if ((jsonArray = jsonObject.getAsJsonArray("createAssetIds")) != null && !jsonArray.isEmpty()) {
                    createAssetIds = new ArrayList();
                    for (int i = 0; i < jsonArray.size(); i++) {
                        assetId = jsonArray.get(i).getAsString();
                        createAssetIds.add(UUID.fromString(assetId));
                    }
                    notificationVO.setCreateAssets(createAssetIds);

                }
            }
            if (jsonObject.has("deleteAssetIds")) {
                if ((jsonArray = jsonObject.getAsJsonArray("deleteAssetIds")) != null && !jsonArray.isEmpty()) {
                    deleteAssetIds = new ArrayList();
                    for (int i = 0; i < jsonArray.size(); i++) {
                        assetId = jsonArray.get(i).getAsString();
                        deleteAssetIds.add(UUID.fromString(assetId));
                    }
                    notificationVO.setDeleteAssets(deleteAssetIds);
                }
            }
        }
        return notificationVO;
    }

    /**
     * Parse the given json String Object and return a PasswordTokenVO object
     *
     * @param content, String Object
     * @return PasswordTokenVO Object
     */
    public static PasswordTokenVO passwordTokenParser(String content) {
        PasswordTokenVO passwordTokenVO = null;
        JsonElement element;
        JsonObject jsonObject;

        if (content != null && !content.isEmpty() && (element = JsonParser.parseString(content)) != null && (jsonObject = element.getAsJsonObject()) != null) {
            passwordTokenVO = new PasswordTokenVO();

            if (jsonObject.has("userId")) {
                try {
                    passwordTokenVO.setUserId(jsonObject.get("userId").getAsString());
                } catch (Exception ex) {
                }
            }
            if (jsonObject.has("passwordToken")) {
                try {
                    passwordTokenVO.setPasswordToken(UUID.fromString(jsonObject.get("passwordToken").getAsString()));
                } catch (Exception ex) {
                }
            }
        }
        return passwordTokenVO;
    }
}
