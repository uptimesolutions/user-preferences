/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.userservice.utils;

import com.uptime.cassandra.userpreferences.entity.AlarmAssetNotification;
import com.uptime.cassandra.userpreferences.entity.AlarmSiteNotification;
import com.uptime.cassandra.userpreferences.entity.AlarmUserAssetNotification;
import com.uptime.cassandra.userpreferences.entity.AlarmUserSiteNotification;
import com.uptime.cassandra.userpreferences.entity.PasswordToken;
import com.uptime.cassandra.userpreferences.entity.SiteUsers;
import com.uptime.cassandra.userpreferences.entity.UserPreferences;
import com.uptime.cassandra.userpreferences.entity.UserSites;
import com.uptime.userservice.vo.AlarmNotificationVO;
import com.uptime.userservice.vo.PasswordTokenVO;
import com.uptime.userservice.vo.SiteUsersVO;
import com.uptime.userservice.vo.UserPreferencesVO;
import com.uptime.userservice.vo.UserSitesVO;

/**
 *
 * @author Joseph
 */
public class DelegateUtil {

    /**
     * Convert UserSitesVO Object to UserSites Object
     *
     * @param userSitesVO, UserSitesVO Object
     * @return UserSites Object
     */
    public static UserSites getUserSites(UserSitesVO userSitesVO) {
        UserSites entity = new UserSites();
        
        if (userSitesVO != null) {
            entity.setCustomerAccount(userSitesVO.getCustomerAccount());
            entity.setSiteId(userSitesVO.getSiteId());
            entity.setSiteRole(userSitesVO.getSiteRole());
            entity.setUserId(userSitesVO.getUserId());
        }
        return entity;
    }

    /**
     * Convert SiteUsersVO Object to SiteUsers Object
     *
     * @param siteUsersVO, SiteUsersVO Object
     * @return UserSites Object
     */
    public static SiteUsers getSiteUsers(SiteUsersVO siteUsersVO) {
        SiteUsers entity = new SiteUsers();
        
        if (siteUsersVO != null) {
            entity.setCustomerAccount(siteUsersVO.getCustomerAccount());
            entity.setSiteId(siteUsersVO.getSiteId());
            entity.setSiteRole(siteUsersVO.getSiteRole());
            entity.setUserId(siteUsersVO.getUserId());
            entity.setFirstName(siteUsersVO.getFirstName());
            entity.setLastName(siteUsersVO.getLastName());
        }
        return entity;
    }

    /**
     * Convert UserPreferencesVO Object to UserPreferences Object
     *
     * @param userPreferencesVO, UserPreferencesVO Object
     * @return UserPreferences Object
     */
    public static UserPreferences getUserPreferences(UserPreferencesVO userPreferencesVO) {
        UserPreferences entity = new UserPreferences();
        
        if (userPreferencesVO != null) {
            entity.setCustomerAccount(userPreferencesVO.getCustomerAccount());
            entity.setDefaultSite(userPreferencesVO.getDefaultSite());
            entity.setDefaultUnits(userPreferencesVO.getDefaultUnits());
            entity.setLanguage(userPreferencesVO.getLanguage());
            entity.setTimezone(userPreferencesVO.getTimezone());
            entity.setUserId(userPreferencesVO.getUserId());
        }
        return entity;
    }

    /**
     * Convert AlarmNotificationVO Object to AlarmUserAssetNotification Object
     *
     * @param alarmNotificationVO, AlarmNotificationVO Object
     * @return AlarmUserAssetNotification Object
     */
    public static AlarmUserAssetNotification getAlarmUserAssetNotification(AlarmNotificationVO alarmNotificationVO) {
        AlarmUserAssetNotification entity = new AlarmUserAssetNotification();
        
        if (alarmNotificationVO != null) {
            entity.setAssetId(alarmNotificationVO.getAssetId());
            entity.setUserId(alarmNotificationVO.getUserId());
            entity.setSiteId(alarmNotificationVO.getSiteId());
            entity.setEmailAddress(alarmNotificationVO.getEmailAddress());
            entity.setAlert(alarmNotificationVO.isAlert());
            entity.setFault(alarmNotificationVO.isFault());
        }
        return entity;
    }

    /**
     * Convert AlarmNotificationVO Object to AlarmUserSiteNotification Object
     *
     * @param alarmNotificationVO, AlarmNotificationVO Object
     * @return AlarmUserSiteNotification Object
     */
    public static AlarmUserSiteNotification getAlarmUserSiteNotification(AlarmNotificationVO alarmNotificationVO) {
        AlarmUserSiteNotification entity = new AlarmUserSiteNotification();
        
        if (alarmNotificationVO != null) {
            entity.setUserId(alarmNotificationVO.getUserId());
            entity.setSiteId(alarmNotificationVO.getSiteId());
            entity.setEmailAddress(alarmNotificationVO.getEmailAddress());
            entity.setAlert(alarmNotificationVO.isAlert());
            entity.setFault(alarmNotificationVO.isFault());
        }
        return entity;
    }

    /**
     * Convert AlarmNotificationVO Object to AlarmAssetNotification Object
     *
     * @param alarmNotificationVO, AlarmNotificationVO Object
     * @return AlarmAssetNotification Object
     */
    public static AlarmAssetNotification getAlarmAssetNotification(AlarmNotificationVO alarmNotificationVO) {
        AlarmAssetNotification entity = new AlarmAssetNotification();
        
        if (alarmNotificationVO != null) {
            entity.setAssetId(alarmNotificationVO.getAssetId());
            entity.setUserId(alarmNotificationVO.getUserId());
            entity.setEmailAddress(alarmNotificationVO.getEmailAddress());
            entity.setAlert(alarmNotificationVO.isAlert());
            entity.setFault(alarmNotificationVO.isFault());
        }
        return entity;
    }

    /**
     * Convert AlarmNotificationVO Object to AlarmSiteNotification Object
     *
     * @param alarmNotificationVO, AlarmNotificationVO Object
     * @return AlarmSiteNotification Object
     */
    public static AlarmSiteNotification getAlarmSiteNotification(AlarmNotificationVO alarmNotificationVO) {
        AlarmSiteNotification entity = new AlarmSiteNotification();
        
        if (alarmNotificationVO != null) {
            entity.setUserId(alarmNotificationVO.getUserId());
            entity.setSiteId(alarmNotificationVO.getSiteId());
            entity.setEmailAddress(alarmNotificationVO.getEmailAddress());
            entity.setAlert(alarmNotificationVO.isAlert());
            entity.setFault(alarmNotificationVO.isFault());
        }
        return entity;
    }

    /**
     * Convert PasswordTokenVO Object to PasswordToken Object
     *
     * @param passwordTokenVO, PasswordTokenVO Object
     * @return PasswordToken Object
     */
    public static PasswordToken getPasswordToken(PasswordTokenVO passwordTokenVO) {
        PasswordToken entity = null;
        
        if (passwordTokenVO != null) {
            entity = new PasswordToken();
            entity.setUserId(passwordTokenVO.getUserId());
            entity.setPasswordToken(passwordTokenVO.getPasswordToken());
        }
        
        return entity;
    }

}
