/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.userservice.vo;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author aswani
 */
public class AlarmNotificationVO {
    private String userId;
    private UUID siteId;
    private UUID assetId;
    private String emailAddress;
    private boolean alert;
    private boolean fault;
    transient private boolean delete = false;
    private List<UUID> createAssets;
    private List<UUID> deleteAssets;

    public AlarmNotificationVO() {
    }

    public AlarmNotificationVO(AlarmNotificationVO notificationVO) {
        this.userId = notificationVO.getUserId();
        this.siteId = notificationVO.getSiteId();
        this.assetId = notificationVO.getAssetId();
        this.emailAddress = notificationVO.getEmailAddress();
        this.alert = notificationVO.isAlert();
        this.fault = notificationVO.isFault();
        this.delete = notificationVO.isDelete();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public UUID getAssetId() {
        return assetId;
    }

    public void setAssetId(UUID assetId) {
        this.assetId = assetId;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public boolean isAlert() {
        return alert;
    }

    public void setAlert(boolean alert) {
        this.alert = alert;
    }

    public boolean isFault() {
        return fault;
    }

    public void setFault(boolean fault) {
        this.fault = fault;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    public boolean isDelete() {
        return delete;
    }

    public List<UUID> getCreateAssets() {
        return createAssets;
    }

    public void setCreateAssets(List<UUID> createAssets) {
        this.createAssets = createAssets;
    }

    public List<UUID> getDeleteAssets() {
        return deleteAssets;
    }

    public void setDeleteAssets(List<UUID> deleteAssets) {
        this.deleteAssets = deleteAssets;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.userId);
        hash = 97 * hash + Objects.hashCode(this.siteId);
        hash = 97 * hash + Objects.hashCode(this.assetId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AlarmNotificationVO other = (AlarmNotificationVO) obj;
        if (this.alert != other.alert) {
            return false;
        }
        if (this.fault != other.fault) {
            return false;
        }
        if (!Objects.equals(this.userId, other.userId)) {
            return false;
        }
        if (!Objects.equals(this.emailAddress, other.emailAddress)) {
            return false;
        }
        if (!Objects.equals(this.siteId, other.siteId)) {
            return false;
        }
        if (!Objects.equals(this.assetId, other.assetId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AlarmNotificationVO{" + "userId=" + userId + ", siteId=" + siteId + ", assetId=" + assetId + ", emailAddress=" + emailAddress + ", alert=" + alert + ", fault=" + fault + ", delete=" + delete + ", createAssets=" + createAssets + ", deleteAssets=" + deleteAssets + '}';
    }

}
