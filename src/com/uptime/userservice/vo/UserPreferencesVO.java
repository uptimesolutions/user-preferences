/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.userservice.vo;

import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author Joseph
 */
public class UserPreferencesVO {
    private String userId;
    private String customerAccount;
    private String language;
    private String timezone;
    private UUID defaultSite;
    private String defaultUnits;

    public UserPreferencesVO() {
    }

    public UserPreferencesVO(UserPreferencesVO userPreferencesVO) {
        userId = userPreferencesVO.getUserId();
        customerAccount = userPreferencesVO.getCustomerAccount();
        language = userPreferencesVO.getLanguage();
        timezone = userPreferencesVO.getTimezone();
        defaultSite = userPreferencesVO.getDefaultSite();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public UUID getDefaultSite() {
        return defaultSite;
    }

    public void setDefaultSite(UUID defaultSite) {
        this.defaultSite = defaultSite;
    }

    public String getDefaultUnits() {
        return defaultUnits;
    }

    public void setDefaultUnits(String defaultUnits) {
        this.defaultUnits = defaultUnits;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.userId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserPreferencesVO other = (UserPreferencesVO) obj;
        if (!Objects.equals(this.userId, other.userId)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.language, other.language)) {
            return false;
        }
        if (!Objects.equals(this.timezone, other.timezone)) {
            return false;
        }
        if (!Objects.equals(this.defaultSite, other.defaultSite)) {
            return false;
        }
        if (!Objects.equals(this.defaultUnits, other.defaultUnits)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UserPreferencesVO{" + "userId=" + userId + ", customerAccount=" + customerAccount + ", language=" + language + ", timezone=" + timezone + ", defaultSite=" + defaultSite + ", defaultUnits=" + defaultUnits + "}";
    }
}
