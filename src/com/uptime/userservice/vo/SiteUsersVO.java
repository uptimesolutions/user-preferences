/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.userservice.vo;

import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author gsingh
 */
public class SiteUsersVO {
    private String userId;
    private String customerAccount;
    private UUID siteId;
    private String siteRole;
    private String firstName;
    private String lastName;

    public SiteUsersVO() {
    }

    public SiteUsersVO(SiteUsersVO userSitesVO) {
        userId = userSitesVO.getUserId();
        customerAccount = userSitesVO.getCustomerAccount();
        siteId = userSitesVO.getSiteId();
        siteRole = userSitesVO.getSiteRole();
        firstName = userSitesVO.getFirstName();
        lastName = userSitesVO.getLastName();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public UUID getSiteId() {
        return siteId;
    }

    public void setSiteId(UUID siteId) {
        this.siteId = siteId;
    }

    public String getSiteRole() {
        return siteRole;
    }

    public void setSiteRole(String siteRole) {
        this.siteRole = siteRole;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.userId);
        hash = 53 * hash + Objects.hashCode(this.customerAccount);
        hash = 53 * hash + Objects.hashCode(this.siteId);
        hash = 53 * hash + Objects.hashCode(this.siteRole);
        hash = 53 * hash + Objects.hashCode(this.firstName);
        hash = 53 * hash + Objects.hashCode(this.lastName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SiteUsersVO other = (SiteUsersVO) obj;
        if (!Objects.equals(this.userId, other.userId)) {
            return false;
        }
        if (!Objects.equals(this.customerAccount, other.customerAccount)) {
            return false;
        }
        if (!Objects.equals(this.siteRole, other.siteRole)) {
            return false;
        }
        if (!Objects.equals(this.firstName, other.firstName)) {
            return false;
        }
        if (!Objects.equals(this.lastName, other.lastName)) {
            return false;
        }
        return Objects.equals(this.siteId, other.siteId);
    }

    @Override
    public String toString() {
        return "SiteUsersVO{" + "userId=" + userId + ", customerAccount=" + customerAccount + ", siteId=" + siteId + ", siteRole=" + siteRole + ", firstName=" + firstName + ", lastName=" + lastName + '}';
    }

}
