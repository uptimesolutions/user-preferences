package com.uptime.userservice.vo;
import java.util.UUID;
/**
 *
 * @author aswan
 */
public class KafkaNotificationVO {
    
    private UUID notificationId;
    private String emailAddress;
    private String notificationType;
    private boolean alert;
    private boolean fault;
    public UUID getNotificationId() {
        return notificationId;
    }
    public void setNotificationId(UUID notificationId) {
        this.notificationId = notificationId;
    }
    public String getEmailAddress() {
        return emailAddress;
    }
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
    public String getNotificationType() {
        return notificationType;
    }
    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }
    public boolean isAlert() {
        return alert;
    }
    public void setAlert(boolean alert) {
        this.alert = alert;
    }
    public boolean isFault() {
        return fault;
    }
    public void setFault(boolean fault) {
        this.fault = fault;
    }
    @Override
    public String toString() {
        return "KafkaNotificationVO{" + "notificationId=" + notificationId + ", emailAddress=" + emailAddress + ", notificationType=" + notificationType + ", alert=" + alert + ", fault=" + fault + '}';
    }
    
}