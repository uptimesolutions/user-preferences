/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.userservice.delegate;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.userpreferences.dao.UserPreferencesDAO;
import com.uptime.cassandra.userpreferences.dao.UserPreferencesMapperImpl;
import com.uptime.cassandra.userpreferences.entity.UserPreferences;
import java.util.List;

/**
 *
 * @author Joseph
 */
public class ReadUserPreferencesDelegate {
    private final UserPreferencesDAO userPreferencesDAO;

    /**
     * Constructor
     */
    public ReadUserPreferencesDelegate() {
        userPreferencesDAO = UserPreferencesMapperImpl.getInstance().userPreferencesDAO();
    }

    /**
     * Return a List of UserPreferences Objects by the given params
     * @param userId, String Object
     * @return List of UserPreferences Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<UserPreferences> findByPK(String userId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return userPreferencesDAO.findByPK(userId);
    }
}
