/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.userservice.delegate;

import com.uptime.cassandra.userpreferences.dao.SiteUsersDAO;
import com.uptime.cassandra.userpreferences.dao.UserPreferencesMapperImpl;
import com.uptime.cassandra.userpreferences.dao.UserSitesDAO;
import com.uptime.cassandra.userpreferences.entity.SiteUsers;
import com.uptime.cassandra.userpreferences.entity.UserSites;
import com.uptime.services.util.ServiceUtil;
import static com.uptime.userservice.UserService.sendEvent;
import com.uptime.userservice.utils.DelegateUtil;
import com.uptime.userservice.vo.SiteUsersVO;
import com.uptime.userservice.vo.UserSitesVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class CreateUserSitesDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateUserSitesDelegate.class.getName());
    private final UserSitesDAO userSitesDAO;
    private final SiteUsersDAO siteUsersDAO;

    /**
     * Constructor
     */
    public CreateUserSitesDelegate() {
        userSitesDAO = UserPreferencesMapperImpl.getInstance().userSitesDAO();
        siteUsersDAO = UserPreferencesMapperImpl.getInstance().siteUsersDAO();
    }

    /**
     * Insert new Rows into Cassandra based on the given object
     *
     * @param userSitesVO, UserSitesVO object
     * @param siteUsersVO, SiteUsersVO object
     * @return String object
     */
    public String createUserSites(UserSitesVO userSitesVO, SiteUsersVO siteUsersVO) throws IllegalArgumentException {
        String errorMsg;
        UserSites userSites;
        SiteUsers siteUsers;
        
        userSites = DelegateUtil.getUserSites(userSitesVO);
        siteUsers = DelegateUtil.getSiteUsers(siteUsersVO);
        
        // Insert the entities into Cassandra
        try {
            if ((errorMsg = ServiceUtil.validateObjectData(userSites)) == null && 
                    (errorMsg = ServiceUtil.validateObjectData(siteUsers)) == null) {
                userSitesDAO.create(userSites);
                siteUsersDAO.create(siteUsers);
            } else {
                return errorMsg ;
            }

            return "{\"outcome\":\"New User Sites / Site Users created successfully.\"}";
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to create new User Sites / Site Users.\"}";
    }
}
