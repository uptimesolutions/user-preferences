/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.userservice.delegate;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.userpreferences.dao.AlarmAssetNotificationDAO;
import com.uptime.cassandra.userpreferences.dao.AlarmSiteNotificationDAO;
import com.uptime.cassandra.userpreferences.dao.AlarmUserAssetNotificationDAO;
import com.uptime.cassandra.userpreferences.dao.AlarmUserSiteNotificationDAO;
import com.uptime.cassandra.userpreferences.dao.UserPreferencesMapperImpl;
import com.uptime.cassandra.userpreferences.entity.AlarmAssetNotification;
import com.uptime.cassandra.userpreferences.entity.AlarmSiteNotification;
import com.uptime.cassandra.userpreferences.entity.AlarmUserAssetNotification;
import com.uptime.cassandra.userpreferences.entity.AlarmUserSiteNotification;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author aswani
 */
public class ReadAlarmNotificationDelegate {
    AlarmUserAssetNotificationDAO alarmUserAssetNotificationDAO;
    AlarmUserSiteNotificationDAO alarmUserSiteNotificationDAO;
    AlarmAssetNotificationDAO alarmAssetNotificationDAO;
    AlarmSiteNotificationDAO alarmSiteNotificationDAO;

    /**
     * Constructor
     */
    public ReadAlarmNotificationDelegate() {
        alarmUserAssetNotificationDAO = UserPreferencesMapperImpl.getInstance().alarmUserAssetNotificationDAO();
        alarmUserSiteNotificationDAO = UserPreferencesMapperImpl.getInstance().alarmUserSiteNotificationDAO();
        alarmAssetNotificationDAO = UserPreferencesMapperImpl.getInstance().alarmAssetNotificationDAO();
        alarmSiteNotificationDAO = UserPreferencesMapperImpl.getInstance().alarmSiteNotificationDAO();
    }
    /**
     * Return a List of AlarmUserAssetNotification Objects by the given params
     *
     * @param userId, String Object
     * @param siteId, UUID Object
     * @param assetId, UUID Object
     * @return List of AlarmUserAssetNotification Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<AlarmUserAssetNotification> getAlarmUserAssetNotificationByUserIdSiteIdAssetId(String userId, UUID siteId, UUID assetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return alarmUserAssetNotificationDAO.findByPK(userId, siteId, assetId);
    }

    /**
     * Return a List of AlarmUserAssetNotification Objects by the given params
     *
     * @param userId, String Object
     * @param siteId, UUID Object
     * @return List of AlarmUserAssetNotification Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<AlarmUserAssetNotification> getAlarmUserAssetNotificationByUserIdSiteId(String userId, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return alarmUserAssetNotificationDAO.findByUseridSite(userId, siteId);
    }

    /**
     * Return a List of AlarmUserSiteNotification Objects by the given params
     *
     * @param userId, String Object
     * @return List of AlarmUserSiteNotification Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<AlarmUserSiteNotification> getAlarmUserSiteNotificationByUserid(String userId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return alarmUserSiteNotificationDAO.findByUserId(userId);
    }
    
    /**
     * Return a List of AlarmUserSiteNotification Objects by the given params
     *
     * @param userId, String Object
     * @param siteId
     * @return List of AlarmUserSiteNotification Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<AlarmUserSiteNotification> getAlarmUserAssetNotificationByUseridSiteId(String userId, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return alarmUserSiteNotificationDAO.findByPK(userId, siteId);
    }
    /**
     * Return a List of AlarmAssetNotification Objects by the given params
     *
     * @param userId, String Object
     * @param assetId 
     * @return List of AlarmAssetNotification Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<AlarmAssetNotification> getAlarmAssetNotificationByUseridAssetId(String userId, UUID assetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return alarmAssetNotificationDAO.findByPK(assetId, userId);
    }

    /**
     * Return a List of AlarmUserSiteNotification Objects by the given params
     *
     * @param userId, String Object
     * @param siteId, UUID Object
     * @return List of AlarmUserSiteNotification Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<AlarmUserSiteNotification> getAlarmUserSiteNotificationByUseridAssetId(String userId, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return alarmUserSiteNotificationDAO.findByPK(userId, siteId);
    }

    /**
     * Return a List of AlarmAssetNotification Objects by the given params
     *
     * @param assetId, UUID Object
     * @param userId, String Object
     * @return List of AlarmAssetNotification Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<AlarmAssetNotification> getAlarmAssetNotificationByAssetIdUserId(UUID assetId, String userId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return alarmAssetNotificationDAO.findByPK(assetId, userId);
    }

    /**
     * Return a List of AlarmAssetNotification Objects by the given params
     *
     * @param assetId, UUID Object
     * @return List of AlarmAssetNotification Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<AlarmAssetNotification> getAlarmAssetNotificationByAsset(UUID assetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return alarmAssetNotificationDAO.findByAsset(assetId);
    }
    
    /**
     * Return a List of AlarmSiteNotification Objects by the given params
     *
     * @param assetId, UUID Object
     * @param userId, String Object
     * @return List of AlarmSiteNotification Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<AlarmSiteNotification> getAlarmSiteNotificationByAssetIdUserId(UUID assetId, String userId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return alarmSiteNotificationDAO.findByPK(assetId, userId);
    }

    /**
     * Return a List of AlarmSiteNotification Objects by the given params
     *
     * @param siteId, UUID Object
     * @return List of AlarmSiteNotification Objects
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<AlarmSiteNotification> getAlarmSiteNotificationByBySite(UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return alarmSiteNotificationDAO.findBySite(siteId);
    }
}
