/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.userservice.delegate;

import com.uptime.cassandra.userpreferences.dao.PasswordTokenDAO;
import com.uptime.cassandra.userpreferences.dao.UserPreferencesMapperImpl;
import com.uptime.cassandra.userpreferences.entity.PasswordToken;
import com.uptime.services.util.ServiceUtil;
import static com.uptime.userservice.UserService.sendEvent;
import com.uptime.userservice.utils.DelegateUtil;
import static com.uptime.userservice.utils.DelegateUtil.getPasswordToken;
import com.uptime.userservice.vo.PasswordTokenVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author twilcox
 */
public class CreatePasswordTokenDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreatePasswordTokenDelegate.class.getName());
    private final PasswordTokenDAO passwordTokenDAO;
    private final int TTL;

    /**
     * Constructor
     */
    public CreatePasswordTokenDelegate() {
        passwordTokenDAO = UserPreferencesMapperImpl.getInstance().passwordTokenDAO();
        TTL = 1800; // 30min
    }

    /**
     * Insert new Row into Cassandra based on the given object
     * @param passwordTokenVO, PasswordTokenVO Object
     * @return String object
     */
    public String create(PasswordTokenVO passwordTokenVO) throws IllegalArgumentException {
        try {
            // *********Not calling the validateObjectData method since password_token table does not have non PK columns*********
            //          if ((errorMsg = ServiceUtil.validateObjectData(passwordToken))== null) {
            // *******************************************************************************************************************
            passwordTokenDAO.create(DelegateUtil.getPasswordToken(passwordTokenVO), TTL);
            return "{\"outcome\":\"New Password Token created successfully.\"}";
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to create new Password Token.\"}";
    }
}
