/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.userservice.delegate;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.userpreferences.dao.PasswordTokenDAO;
import com.uptime.cassandra.userpreferences.dao.UserPreferencesMapperImpl;
import com.uptime.cassandra.userpreferences.entity.PasswordToken;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author twilcox
 */
public class ReadPasswordTokenDelegate {
    private final PasswordTokenDAO passwordTokenDAO;

    /**
     * Constructor
     */
    public ReadPasswordTokenDelegate() {
        passwordTokenDAO = UserPreferencesMapperImpl.getInstance().passwordTokenDAO();
    }
    
    /**
     * Return a List of PasswordToken Objects by the given params
     * @param userId, String Object
     * @param passwordToken, UUID Object
     * @return List Object of PasswordToken Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<PasswordToken> getPasswordTokenByPK(String userId, UUID passwordToken) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return passwordTokenDAO.findByPK(userId, passwordToken);
    }
}
