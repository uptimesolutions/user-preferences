/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.userservice.delegate;

import com.uptime.cassandra.userpreferences.dao.AlarmAssetNotificationDAO;
import com.uptime.cassandra.userpreferences.dao.AlarmSiteNotificationDAO;
import com.uptime.cassandra.userpreferences.dao.AlarmUserAssetNotificationDAO;
import com.uptime.cassandra.userpreferences.dao.AlarmUserSiteNotificationDAO;
import com.uptime.cassandra.userpreferences.dao.UserPreferencesMapperImpl;
import com.uptime.cassandra.userpreferences.entity.AlarmAssetNotification;
import com.uptime.cassandra.userpreferences.entity.AlarmSiteNotification;
import com.uptime.cassandra.userpreferences.entity.AlarmUserAssetNotification;
import com.uptime.cassandra.userpreferences.entity.AlarmUserSiteNotification;
import com.uptime.services.util.ServiceUtil;
import com.uptime.userservice.UserService;
import static com.uptime.userservice.UserService.sendEvent;
import com.uptime.userservice.utils.DelegateUtil;
import com.uptime.userservice.vo.AlarmNotificationVO;
import com.uptime.userservice.vo.KafkaNotificationVO;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author aswani
 */
public class CreateAlarmNotificationDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateAlarmNotificationDelegate.class.getName());
    AlarmUserAssetNotificationDAO alarmUserAssetNotificationDAO;
    AlarmUserSiteNotificationDAO alarmUserSiteNotificationDAO;
    AlarmAssetNotificationDAO alarmAssetNotificationDAO;
    AlarmSiteNotificationDAO alarmSiteNotificationDAO;

    public CreateAlarmNotificationDelegate() {
        alarmUserAssetNotificationDAO = UserPreferencesMapperImpl.getInstance().alarmUserAssetNotificationDAO();
        alarmUserSiteNotificationDAO = UserPreferencesMapperImpl.getInstance().alarmUserSiteNotificationDAO();
        alarmAssetNotificationDAO = UserPreferencesMapperImpl.getInstance().alarmAssetNotificationDAO();
        alarmSiteNotificationDAO = UserPreferencesMapperImpl.getInstance().alarmSiteNotificationDAO();
    }

    /**
     * Insert new Rows into Cassandra based on the given object
     *
     * @param notificationVO AlarmNotificationVO object
     * @return String object
     * @throws Exception
     */
    public String createAlarmNotification(AlarmNotificationVO notificationVO) throws IllegalArgumentException, Exception {
        AlarmUserAssetNotification alarmUserAssetNotification;
        AlarmAssetNotification alarmAssetNotification;
        AlarmUserSiteNotification alarmUserSiteNotification;
        AlarmSiteNotification alarmSiteNotification;

        List<AlarmUserAssetNotification> createAlarmUserAssetNotificationList;
        List<AlarmAssetNotification> createAlarmAssetNotificationList;

        String errorMsg;

        // if Asset Id is null
        if (notificationVO.getCreateAssets() == null) {
            alarmUserSiteNotification = DelegateUtil.getAlarmUserSiteNotification(notificationVO);
            alarmSiteNotification = DelegateUtil.getAlarmSiteNotification(notificationVO);

            // Insert the entities into Cassandra
            try {
                if (((errorMsg = ServiceUtil.validateObjectData(alarmUserSiteNotification)) == null)
                        && ((errorMsg = ServiceUtil.validateObjectData(alarmSiteNotification)) == null)) {
                    alarmUserSiteNotificationDAO.create(alarmUserSiteNotification);
                    alarmSiteNotificationDAO.create(alarmSiteNotification);
                    
                    // Send kafka message
                    KafkaNotificationVO kafkaNotificationVO = new KafkaNotificationVO();
                    kafkaNotificationVO.setNotificationId(alarmSiteNotification.getSiteId());
                    kafkaNotificationVO.setNotificationType("site_id");
                    kafkaNotificationVO.setEmailAddress(alarmSiteNotification.getEmailAddress());
                    kafkaNotificationVO.setAlert(alarmSiteNotification.isAlert());
                    kafkaNotificationVO.setFault(alarmSiteNotification.isFault());
                    UserService.sendKafkaAlarmNotificationMessage(kafkaNotificationVO);
                    
                } else {
                    return errorMsg;
                }
                return "{\"outcome\":\"New Alarm Notification items created successfully.\"}";
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
        } // if Asset Id isn't null
        else {
            createAlarmUserAssetNotificationList = new ArrayList();
            createAlarmAssetNotificationList = new ArrayList();
            
            if (notificationVO.getCreateAssets() != null && !notificationVO.getCreateAssets().isEmpty()) {
                for (UUID assetId : notificationVO.getCreateAssets()) {
                    notificationVO.setAssetId(assetId);
                    alarmUserAssetNotification = DelegateUtil.getAlarmUserAssetNotification(notificationVO);
                    alarmAssetNotification = DelegateUtil.getAlarmAssetNotification(notificationVO);
                    if (((errorMsg = ServiceUtil.validateObjectData(alarmUserAssetNotification)) == null)
                            && ((errorMsg = ServiceUtil.validateObjectData(alarmAssetNotification)) == null)) {
                        createAlarmUserAssetNotificationList.add(alarmUserAssetNotification);
                        createAlarmAssetNotificationList.add(alarmAssetNotification);
                    } else {
                        return errorMsg;
                    }
                }
            }
            
            // Insert the entities into Cassandra
            try {
                if (!createAlarmUserAssetNotificationList.isEmpty() && !createAlarmAssetNotificationList.isEmpty()) {
                    alarmUserAssetNotificationDAO.create(createAlarmUserAssetNotificationList);
                    alarmAssetNotificationDAO.create(createAlarmAssetNotificationList);
                    
                    // Send kafka message
                    createAlarmUserAssetNotificationList.forEach(notification -> {
                        KafkaNotificationVO kafkaNotificationVO = new KafkaNotificationVO();
                        kafkaNotificationVO.setNotificationId(notification.getAssetId());
                        kafkaNotificationVO.setNotificationType("asset_id");
                        kafkaNotificationVO.setEmailAddress(notification.getEmailAddress());
                        kafkaNotificationVO.setAlert(notification.isAlert());
                        kafkaNotificationVO.setFault(notification.isFault());
                        UserService.sendKafkaAlarmNotificationMessage(kafkaNotificationVO);
                    });
                }
                return "{\"outcome\":\"New Alarm Notification items created successfully.\"}";
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
        }

        return "{\"outcome\":\"Error: Failed to create new Alarm Notification items.\"}";
    }
}
