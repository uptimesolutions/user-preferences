/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.userservice.delegate;

import com.uptime.cassandra.userpreferences.dao.UserPreferencesDAO;
import com.uptime.cassandra.userpreferences.dao.UserPreferencesMapperImpl;
import com.uptime.cassandra.userpreferences.entity.UserPreferences;
import com.uptime.services.util.ServiceUtil;
import static com.uptime.userservice.UserService.sendEvent;
import com.uptime.userservice.utils.DelegateUtil;
import com.uptime.userservice.vo.UserPreferencesVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author Joseph
 */
public class CreateUserPreferencesDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateUserPreferencesDelegate.class.getName());
    private final UserPreferencesDAO userPreferencesDAO;

    public CreateUserPreferencesDelegate() {
        userPreferencesDAO = UserPreferencesMapperImpl.getInstance().userPreferencesDAO();
    }

    /**
     * Insert new Rows into Cassandra based on the given object
     *
     * @param userPreferencesVO, UserPreferencesVO object
     * @return String object
     */
    public String createUserPreferences(UserPreferencesVO userPreferencesVO) throws IllegalArgumentException {
        UserPreferences userPreferences;
        String errorMsg;
        
        userPreferences = DelegateUtil.getUserPreferences(userPreferencesVO);
        
        // Insert the entities into Cassandra
        try {
            if ((errorMsg = ServiceUtil.validateObjectData(userPreferences)) == null) {
                userPreferencesDAO.create(userPreferences);
            } else {
                return errorMsg;
            }
            return "{\"outcome\":\"New User Preferences created successfully.\"}";
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to create new User Preferences.\"}";
    }
}
