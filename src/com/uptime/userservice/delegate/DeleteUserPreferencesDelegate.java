/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.userservice.delegate;

import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.uptime.cassandra.userpreferences.dao.UserPreferencesDAO;
import com.uptime.cassandra.userpreferences.dao.UserPreferencesMapperImpl;
import com.uptime.cassandra.userpreferences.entity.UserPreferences;
import static com.uptime.userservice.UserService.sendEvent;
import com.uptime.userservice.vo.UserPreferencesVO;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class DeleteUserPreferencesDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteUserPreferencesDelegate.class.getName());
    private final UserPreferencesDAO userPreferencesDAO;

    /**
     * Constructor
     */
    public DeleteUserPreferencesDelegate() {
        userPreferencesDAO = UserPreferencesMapperImpl.getInstance().userPreferencesDAO();
    }

    /**
     * Delete Rows from Cassandra with containing the given values
     *
     * @param userPreferencesVO, UserPreferencesVO Object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String deleteUserPreferences(UserPreferencesVO userPreferencesVO) throws IllegalArgumentException, Exception {
        List<UserPreferences> userPreferencesList;
        UserPreferences userPreferences = null;

        // Find the entities based on the given values
        try {
            if ((userPreferencesList = userPreferencesDAO.findByPK(userPreferencesVO.getUserId())) != null && !userPreferencesList.isEmpty()) {
                userPreferences = userPreferencesList.get(0);
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find User Preferences items to delete.\"}";
        }

        // Delete the entities from Cassandra
        try {
            if (userPreferences != null) {
                userPreferencesDAO.delete(userPreferences);
                return "{\"outcome\":\"Deleted User Preferences items successfully.\"}";
            } else {
                return "{\"outcome\":\"No User Preferences items found to delete.\"}";
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (UnavailableException | WriteTimeoutException e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to delete User Preferences items.\"}";
    }
}
