/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.userservice.delegate;

import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.uptime.cassandra.userpreferences.dao.AlarmAssetNotificationDAO;
import com.uptime.cassandra.userpreferences.dao.AlarmSiteNotificationDAO;
import com.uptime.cassandra.userpreferences.dao.AlarmUserAssetNotificationDAO;
import com.uptime.cassandra.userpreferences.dao.AlarmUserSiteNotificationDAO;
import com.uptime.cassandra.userpreferences.dao.UserPreferencesMapperImpl;
import com.uptime.cassandra.userpreferences.entity.AlarmAssetNotification;
import com.uptime.cassandra.userpreferences.entity.AlarmSiteNotification;
import com.uptime.cassandra.userpreferences.entity.AlarmUserAssetNotification;
import com.uptime.cassandra.userpreferences.entity.AlarmUserSiteNotification;
import com.uptime.userservice.UserService;
import static com.uptime.userservice.UserService.sendEvent;
import com.uptime.userservice.vo.AlarmNotificationVO;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author aswani
 */
public class DeleteAlarmNotificationDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteAlarmNotificationDelegate.class.getName());
    AlarmUserAssetNotificationDAO alarmUserAssetNotificationDAO;
    AlarmUserSiteNotificationDAO alarmUserSiteNotificationDAO;
    AlarmAssetNotificationDAO alarmAssetNotificationDAO;
    AlarmSiteNotificationDAO alarmSiteNotificationDAO;

    /**
     * Constructor
     */
    public DeleteAlarmNotificationDelegate() {
        alarmUserAssetNotificationDAO = UserPreferencesMapperImpl.getInstance().alarmUserAssetNotificationDAO();
        alarmUserSiteNotificationDAO = UserPreferencesMapperImpl.getInstance().alarmUserSiteNotificationDAO();
        alarmAssetNotificationDAO = UserPreferencesMapperImpl.getInstance().alarmAssetNotificationDAO();
        alarmSiteNotificationDAO = UserPreferencesMapperImpl.getInstance().alarmSiteNotificationDAO();
    }

    /**
     * Delete Row from Cassandra based for the given object
     *
     * @param notificationVO AlarmNotificationVO object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String deleteAlarmNotification(AlarmNotificationVO notificationVO) throws IllegalArgumentException, Exception {
        List<AlarmUserAssetNotification> alarmUserAssetNotificationList;
        List<AlarmAssetNotification> alarmAssetNotificationList;
        List<AlarmUserSiteNotification> alarmUserSiteNotificationList;
        List<AlarmSiteNotification> alarmSiteNotificationList;

        AlarmUserAssetNotification alarmUserAssetNotification = null;
        AlarmAssetNotification alarmAssetNotification = null;
        AlarmUserSiteNotification alarmUserSiteNotification = null;
        AlarmSiteNotification alarmSiteNotification = null;

        List<AlarmUserAssetNotification> deleteAlarmUserAssetNotificationList;
        List<AlarmAssetNotification> deleteAlarmAssetNotificationList;

        // if Asset Id is null
        if (notificationVO.getAssetId() == null) {
            // Find the entities based on the given values
            try {
                if ((alarmUserSiteNotificationList = alarmUserSiteNotificationDAO.findByPK(notificationVO.getUserId(), notificationVO.getSiteId())) != null && !alarmUserSiteNotificationList.isEmpty()) {
                    alarmUserSiteNotification = alarmUserSiteNotificationList.get(0);
                }
                if ((alarmSiteNotificationList = alarmSiteNotificationDAO.findByPK(notificationVO.getSiteId(), notificationVO.getUserId())) != null && !alarmSiteNotificationList.isEmpty()) {
                    alarmSiteNotification = alarmSiteNotificationList.get(0);
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
                return "{\"outcome\":\"Error: Failed to find Alarm Notification items to delete.\"}";
            }

            // Delete the entities from Cassandra
            try {
                if (alarmUserSiteNotification != null && alarmSiteNotification != null) {
                    alarmUserSiteNotificationDAO.delete(alarmUserSiteNotification);
                    alarmSiteNotificationDAO.delete(alarmSiteNotification);
                    UserService.sendKafkaAlarmNotificationMessage(alarmSiteNotification.getSiteId(), "site_id");
                    return "{\"outcome\":\"Alarm Notification deleted successfully.\"}";
                } else {
                    return "{\"outcome\":\"No Alarm Notification items found to delete.\"}";
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (UnavailableException | WriteTimeoutException e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
        } else {// if Asset Id isn't null
            deleteAlarmUserAssetNotificationList = new ArrayList();
            deleteAlarmAssetNotificationList = new ArrayList();
            
            // Find the entities based on the given values
            if (notificationVO.getDeleteAssets() != null && !notificationVO.getDeleteAssets().isEmpty()) {
                for (UUID assetId : notificationVO.getDeleteAssets()) {
                    // Find the entities based on the given values
                    try {
                        if ((alarmUserAssetNotificationList = alarmUserAssetNotificationDAO.findByPK(notificationVO.getUserId(), notificationVO.getSiteId(), assetId)) != null && !alarmUserAssetNotificationList.isEmpty()) {
                            alarmUserAssetNotification = alarmUserAssetNotificationList.get(0);
                        }
                        if ((alarmAssetNotificationList = alarmAssetNotificationDAO.findByPK(assetId, notificationVO.getUserId())) != null && !alarmAssetNotificationList.isEmpty()) {
                            alarmAssetNotification = alarmAssetNotificationList.get(0);
                        }
                    } catch (IllegalArgumentException e) {
                        throw e;
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                        sendEvent(e.getStackTrace());
                        return "{\"outcome\":\"Error: Failed to find Alarm Notification items to delete.\"}";
                    }

                    if (alarmUserAssetNotification != null && alarmAssetNotification != null) {
                        deleteAlarmUserAssetNotificationList.add(alarmUserAssetNotification);
                        deleteAlarmAssetNotificationList.add(alarmAssetNotification);
                    }
                }
            }

            // Delete the entities from Cassandra
            try {
                if (!deleteAlarmUserAssetNotificationList.isEmpty() && !deleteAlarmAssetNotificationList.isEmpty()) {
                    // Delete the entities from Cassandra
                    alarmUserAssetNotificationDAO.delete(deleteAlarmUserAssetNotificationList);
                    alarmAssetNotificationDAO.delete(deleteAlarmAssetNotificationList);
                    
                    //Send delete kafka notification
                    deleteAlarmUserAssetNotificationList.forEach(notification -> {
                        UserService.sendKafkaAlarmNotificationMessage(notification.getAssetId(), "asset_id");
                    });
                    return "{\"outcome\":\"Alarm Notification deleted successfully.\"}";
                } else {
                    return "{\"outcome\":\"No Alarm Notification items found to delete.\"}";
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (UnavailableException | WriteTimeoutException e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
        }

        return "{\"outcome\":\"Error: Failed to delete Alarm Notification items.\"}";
    }
    
    /**
     * Delete Row from Cassandra based for the given object
     *
     * @param notificationVO AlarmNotificationVO object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public void deleteAlarmNotificationWithUserDeletion(AlarmNotificationVO notificationVO) throws IllegalArgumentException, Exception {
        List<AlarmUserSiteNotification> alarmUserSiteNotificationList;
        List<AlarmSiteNotification> alarmSiteNotificationList;
        AlarmUserSiteNotification alarmUserSiteNotification = null;
        AlarmSiteNotification alarmSiteNotification = null;
        boolean flag = true;
        
        try {
            if ((alarmUserSiteNotificationList = alarmUserSiteNotificationDAO.findByPK(notificationVO.getUserId(), notificationVO.getSiteId())) != null && !alarmUserSiteNotificationList.isEmpty()) {
                alarmUserSiteNotification = alarmUserSiteNotificationList.get(0);
            }
            if ((alarmSiteNotificationList = alarmSiteNotificationDAO.findByPK(notificationVO.getSiteId(), notificationVO.getUserId())) != null && !alarmSiteNotificationList.isEmpty()) {
                alarmSiteNotification = alarmSiteNotificationList.get(0);
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        
        // Delete the entities from Cassandra
        try {
            if (alarmUserSiteNotification != null && alarmSiteNotification != null) {
                alarmUserSiteNotificationDAO.delete(alarmUserSiteNotification);
                alarmSiteNotificationDAO.delete(alarmSiteNotification);
                UserService.sendKafkaAlarmNotificationMessage(alarmUserSiteNotification.getSiteId(), "site_id");
                flag = false;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (UnavailableException | WriteTimeoutException e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        
        if (flag) {
            List<AlarmUserAssetNotification> deleteAlarmUserAssetNotificationList;
            List<AlarmAssetNotification> deleteAlarmAssetNotificationList = new ArrayList();
            deleteAlarmUserAssetNotificationList = new ReadAlarmNotificationDelegate().getAlarmUserAssetNotificationByUserIdSiteId(notificationVO.getUserId(), notificationVO.getSiteId());
            for (AlarmUserAssetNotification auan : deleteAlarmUserAssetNotificationList) {
                AlarmAssetNotification aan = new AlarmAssetNotification();
                aan.setAssetId(auan.getAssetId());
                aan.setUserId(auan.getUserId());
                deleteAlarmAssetNotificationList.add(aan);
            }
            // Delete the entities from Cassandra
            try {
                if (!deleteAlarmUserAssetNotificationList.isEmpty() && !deleteAlarmAssetNotificationList.isEmpty()) {
                    // Delete the entities from Cassandra
                    alarmUserAssetNotificationDAO.delete(deleteAlarmUserAssetNotificationList);
                    alarmAssetNotificationDAO.delete(deleteAlarmAssetNotificationList);
                    
                    deleteAlarmUserAssetNotificationList.forEach(notification -> {
                        UserService.sendKafkaAlarmNotificationMessage(notification.getAssetId(), "asset_id");
                    });
                }
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (UnavailableException | WriteTimeoutException e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
        }
    }
}
