/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.userservice.delegate;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.userpreferences.dao.SiteUsersDAO;
import com.uptime.cassandra.userpreferences.dao.UserPreferencesMapperImpl;
import com.uptime.cassandra.userpreferences.dao.UserSitesDAO;
import com.uptime.cassandra.userpreferences.entity.SiteUsers;
import com.uptime.cassandra.userpreferences.entity.UserSites;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author Joseph
 */
public class ReadUserSitesDelegate {
    private final UserSitesDAO userSitesDAO;
    private final SiteUsersDAO siteUsersDAO;

    /**
     * Constructor
     */
    public ReadUserSitesDelegate() {
        userSitesDAO = UserPreferencesMapperImpl.getInstance().userSitesDAO();
        siteUsersDAO = UserPreferencesMapperImpl.getInstance().siteUsersDAO();
    }

    /**
     * Return a List of UserSites Objects by the given params
     * @param userId, String Object
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return List of UserSites Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<UserSites> findUserSitesByPK(String userId, String customerAccount, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return userSitesDAO.findByPK(userId, customerAccount, siteId);
    }
    
    
    /**
     * Return a List of SiteUsers Objects by the given params
     * @param userId, String Object
     * @param customerAccount, String Object
     * @param siteId, UUID Object
     * @return List of SiteUsers Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteUsers> findSiteUsersByPK(String userId, String customerAccount, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteUsersDAO.findByPK(customerAccount, siteId, userId);
    }

    /**
     * Return a List of UserSites Objects by the given params
     * @param userId, String Object
     * @param customerAccount, String Object
     * @return List of UserSites Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<UserSites> findByUserIdcustomerAccount(String userId, String customerAccount) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return userSitesDAO.findByUserIdCustomerAccount(userId, customerAccount);
    }
    
    /**
     * Return a List of SiteUsers Objects by the given params
     * @param siteId, String Object
     * @param customerAccount, String Object
     * @return List of SiteUsers Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteUsers> findBysiteIdcustomerAccount(String customerAccount, UUID siteId ) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteUsersDAO.findByCustomerSite(customerAccount, siteId);
    }
    
    /**
     * Return a List of UserSites Objects by the given params
     * @param userId, String Object
     * @return List of UserSites Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<UserSites> findByUserId(String userId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return userSitesDAO.findByUserId(userId);
    }

}
