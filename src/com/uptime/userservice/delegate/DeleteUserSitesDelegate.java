/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.userservice.delegate;

import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.uptime.cassandra.userpreferences.dao.SiteUsersDAO;
import com.uptime.cassandra.userpreferences.dao.UserPreferencesMapperImpl;
import com.uptime.cassandra.userpreferences.dao.UserSitesDAO;
import com.uptime.cassandra.userpreferences.entity.SiteUsers;
import com.uptime.cassandra.userpreferences.entity.UserSites;
import static com.uptime.userservice.UserService.sendEvent;
import com.uptime.userservice.vo.SiteUsersVO;
import com.uptime.userservice.vo.UserSitesVO;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class DeleteUserSitesDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteUserSitesDelegate.class.getName());
    private final UserSitesDAO userSitesDAO;
    private final SiteUsersDAO siteUsersDAO;

    /**
     * Constructor
     */
    public DeleteUserSitesDelegate() {
        userSitesDAO = UserPreferencesMapperImpl.getInstance().userSitesDAO();
        siteUsersDAO = UserPreferencesMapperImpl.getInstance().siteUsersDAO();
    }

    /**
     * Delete Rows from Cassandra with containing the given values
     * @param userSitesVO, UserSitesVO Object
     * @param siteUsersVO, SiteUsersVO Object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String deleteUserSites(UserSitesVO userSitesVO, SiteUsersVO siteUsersVO) throws IllegalArgumentException, Exception {
        List<UserSites> userSitesList;
        List<SiteUsers> siteUsersList;

        UserSites userSites = null;
        SiteUsers siteUsers = null;

        // Find the entities based on the given values
        try {
            if ((userSitesList = userSitesDAO.findByPK(userSitesVO.getUserId() ,userSitesVO.getCustomerAccount(), userSitesVO.getSiteId())) != null && !userSitesList.isEmpty()) {
                userSites = userSitesList.get(0);
            }
            if ((siteUsersList = siteUsersDAO.findByPK(siteUsersVO.getCustomerAccount(), siteUsersVO.getSiteId(), siteUsersVO.getUserId())) != null && !siteUsersList.isEmpty()) {
                siteUsers = siteUsersList.get(0);
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
            return "{\"outcome\":\"Error: Failed to find User Sites items to delete.\"}";
        }

        // Delete the entities from Cassandra
        try {
            if (userSites != null  && siteUsers != null) {
                userSitesDAO.delete(userSites);
                siteUsersDAO.delete(siteUsers);
                return "{\"outcome\":\"Deleted User Sites & Site Users items successfully.\"}";
            } else {
                return "{\"outcome\":\"No User Sites & Site Users items found to delete.\"}";
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (UnavailableException | WriteTimeoutException e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to delete User Sites & Site Users items.\"}";
    }
}
