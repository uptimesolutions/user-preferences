/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.userservice.delegate;

import com.uptime.cassandra.userpreferences.dao.UserPreferencesDAO;
import com.uptime.cassandra.userpreferences.dao.UserPreferencesMapperImpl;
import com.uptime.cassandra.userpreferences.entity.UserPreferences;
import com.uptime.services.util.ServiceUtil;
import static com.uptime.userservice.UserService.sendEvent;
import com.uptime.userservice.utils.DelegateUtil;
import com.uptime.userservice.vo.UserPreferencesVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class UpdateUserPreferencesDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateUserPreferencesDelegate.class.getName());
    private final UserPreferencesDAO userPreferencesDAO;

    /**
     * Constructor
     */
    public UpdateUserPreferencesDelegate() {
        userPreferencesDAO = UserPreferencesMapperImpl.getInstance().userPreferencesDAO();
    }

    /**
     * Update Rows in Cassandra based on the given object
     *
     * @param userPreferencesVO, UserPreferencesVO object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String updateUserPreferences(UserPreferencesVO userPreferencesVO) throws IllegalArgumentException, Exception {
        UserPreferences userPreferences;
        String errorMsg;
        
        // Insert updated entities into Cassandra 
        userPreferences = DelegateUtil.getUserPreferences(userPreferencesVO);

        try {
            if ((errorMsg = ServiceUtil.validateObjectData(userPreferences)) == null) {
                userPreferencesDAO.update(userPreferences);
                return "{\"outcome\":\"Updated User Preferences items successfully.\"}";
            } else {
                return errorMsg;
            }
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to update User Preferences items.\"}";
    }
}
