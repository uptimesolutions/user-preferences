/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.userservice.delegate;

import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.datastax.oss.driver.api.core.servererrors.WriteTimeoutException;
import com.uptime.cassandra.userpreferences.dao.AlarmAssetNotificationDAO;
import com.uptime.cassandra.userpreferences.dao.AlarmSiteNotificationDAO;
import com.uptime.cassandra.userpreferences.dao.AlarmUserAssetNotificationDAO;
import com.uptime.cassandra.userpreferences.dao.AlarmUserSiteNotificationDAO;
import com.uptime.cassandra.userpreferences.dao.UserPreferencesMapperImpl;
import com.uptime.cassandra.userpreferences.entity.AlarmAssetNotification;
import com.uptime.cassandra.userpreferences.entity.AlarmSiteNotification;
import com.uptime.cassandra.userpreferences.entity.AlarmUserAssetNotification;
import com.uptime.cassandra.userpreferences.entity.AlarmUserSiteNotification;
import com.uptime.services.util.ServiceUtil;
import com.uptime.userservice.UserService;
import static com.uptime.userservice.UserService.sendEvent;
import com.uptime.userservice.utils.DelegateUtil;
import com.uptime.userservice.vo.AlarmNotificationVO;
import com.uptime.userservice.vo.KafkaNotificationVO;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author aswani
 */
public class UpdateAlarmNotificationDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateAlarmNotificationDelegate.class.getName());
    AlarmUserAssetNotificationDAO alarmUserAssetNotificationDAO;
    AlarmUserSiteNotificationDAO alarmUserSiteNotificationDAO;
    AlarmAssetNotificationDAO alarmAssetNotificationDAO;
    AlarmSiteNotificationDAO alarmSiteNotificationDAO;

    /**
     * Constructor
     */
    public UpdateAlarmNotificationDelegate() {
        alarmUserAssetNotificationDAO = UserPreferencesMapperImpl.getInstance().alarmUserAssetNotificationDAO();
        alarmUserSiteNotificationDAO = UserPreferencesMapperImpl.getInstance().alarmUserSiteNotificationDAO();
        alarmAssetNotificationDAO = UserPreferencesMapperImpl.getInstance().alarmAssetNotificationDAO();
        alarmSiteNotificationDAO = UserPreferencesMapperImpl.getInstance().alarmSiteNotificationDAO();
    }

    /**
     * Update Row in Cassandra based on the given object
     *
     * @param notificationVO AlarmNotificationVO object
     * @return String object
     * @throws Exception
     */
    public String updateAlarmNotification(AlarmNotificationVO notificationVO) throws IllegalArgumentException, Exception {
        List<AlarmUserAssetNotification> alarmUserAssetNotificationList;
        List<AlarmAssetNotification> alarmAssetNotificationList;

        AlarmUserAssetNotification alarmUserAssetNotification = null;
        AlarmAssetNotification alarmAssetNotification = null;
        AlarmUserSiteNotification alarmUserSiteNotification = null, deleteAlarmUserSiteNotification = null;
        AlarmSiteNotification alarmSiteNotification = null, deleteAlarmSiteNotification = null;

        List<AlarmUserSiteNotification> alarmUserSiteNotificationList;
        List<AlarmSiteNotification> alarmSiteNotificationList;
        List<AlarmUserAssetNotification> createAlarmUserAssetNotificationList, deleteAlarmUserAssetNotificationList;
        List<AlarmAssetNotification> createAlarmAssetNotificationList, deleteAlarmAssetNotificationList;

        String errorMsg;
        deleteAlarmUserAssetNotificationList = new ArrayList();
        deleteAlarmAssetNotificationList = new ArrayList();

        //If assetId is Null
        if (notificationVO.getAssetId() == null) {
            
            //Check for delete.Assets to be deleted
            if (notificationVO.getDeleteAssets() != null && !notificationVO.getDeleteAssets().isEmpty()) {
                for (UUID assetId : notificationVO.getDeleteAssets()) {
                    // Find the entities based on the given values
                    try {
                        if ((alarmUserAssetNotificationList = alarmUserAssetNotificationDAO.findByPK(notificationVO.getUserId(), notificationVO.getSiteId(), assetId)) != null && !alarmUserAssetNotificationList.isEmpty()) {
                            alarmUserAssetNotification = alarmUserAssetNotificationList.get(0);
                        }
                        if ((alarmAssetNotificationList = alarmAssetNotificationDAO.findByPK(assetId, notificationVO.getUserId())) != null && !alarmAssetNotificationList.isEmpty()) {
                            alarmAssetNotification = alarmAssetNotificationList.get(0);
                        }
                    } catch (IllegalArgumentException e) {
                        throw e;
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                        sendEvent(e.getStackTrace());
                        return "{\"outcome\":\"Error: Failed to find Alarm Notification items to update.\"}";
                    }

                    if (alarmUserAssetNotification != null && alarmAssetNotification != null) {
                        deleteAlarmUserAssetNotificationList.add(alarmUserAssetNotification);
                        deleteAlarmAssetNotificationList.add(alarmAssetNotification);
                    }
                }
            }
            
            //alarms to be updated
            alarmUserSiteNotification = DelegateUtil.getAlarmUserSiteNotification(notificationVO);
            alarmSiteNotification = DelegateUtil.getAlarmSiteNotification(notificationVO);

            //perform CRUD operation
            try {
                if ((errorMsg = ServiceUtil.validateObjectData(alarmUserSiteNotification)) == null
                        && (errorMsg = ServiceUtil.validateObjectData(alarmSiteNotification)) == null) {
                    alarmUserSiteNotificationDAO.create(alarmUserSiteNotification);
                    alarmSiteNotificationDAO.create(alarmSiteNotification);
                   
                    // Send kafka message
                    KafkaNotificationVO kafkaNotificationVO = new KafkaNotificationVO();
                    kafkaNotificationVO.setNotificationId(alarmSiteNotification.getSiteId());
                    kafkaNotificationVO.setNotificationType("site_id");
                    kafkaNotificationVO.setEmailAddress(alarmSiteNotification.getEmailAddress());
                    kafkaNotificationVO.setAlert(alarmSiteNotification.isAlert());
                    kafkaNotificationVO.setFault(alarmSiteNotification.isFault());
                    UserService.sendKafkaAlarmNotificationMessage(kafkaNotificationVO);
                } else {
                    return errorMsg;
                }

                if (!deleteAlarmUserAssetNotificationList.isEmpty() && !deleteAlarmAssetNotificationList.isEmpty()) {
                    // Delete the entities from Cassandra
                    alarmUserAssetNotificationDAO.delete(deleteAlarmUserAssetNotificationList);
                    alarmAssetNotificationDAO.delete(deleteAlarmAssetNotificationList);

                    // Send delete kafka message
                    deleteAlarmUserAssetNotificationList.forEach(notification -> {
                        UserService.sendKafkaAlarmNotificationMessage(notification.getAssetId(), "asset_id");
                    });

                }
                return "{\"outcome\":\"Alarm Notification updated successfully.\"}";

            } catch (IllegalArgumentException e) {
                throw e;
            } catch (UnavailableException | WriteTimeoutException e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }
        } //If assetId is Not Null
        else {
            createAlarmUserAssetNotificationList = new ArrayList();
            createAlarmAssetNotificationList = new ArrayList();

            //Assets to be updated
            if (notificationVO.getCreateAssets() != null && !notificationVO.getCreateAssets().isEmpty()) {
                for (UUID assetId : notificationVO.getCreateAssets()) {
                    notificationVO.setAssetId(assetId);
                    alarmUserAssetNotification = DelegateUtil.getAlarmUserAssetNotification(notificationVO);
                    alarmAssetNotification = DelegateUtil.getAlarmAssetNotification(notificationVO);
                    if ((errorMsg = ServiceUtil.validateObjectData(alarmUserAssetNotification)) == null
                            && (errorMsg = ServiceUtil.validateObjectData(alarmAssetNotification)) == null) {
                        createAlarmUserAssetNotificationList.add(alarmUserAssetNotification);
                        createAlarmAssetNotificationList.add(alarmAssetNotification);
                    } else {
                        return errorMsg;
                    }
                }
            }
            //Assets to be deleted
            if (notificationVO.getDeleteAssets() != null && !notificationVO.getDeleteAssets().isEmpty()) {
                for (UUID assetId : notificationVO.getDeleteAssets()) {
                    // Find the entities based on the given values
                    try {
                        if ((alarmUserAssetNotificationList = alarmUserAssetNotificationDAO.findByPK(notificationVO.getUserId(), notificationVO.getSiteId(), assetId)) != null && !alarmUserAssetNotificationList.isEmpty()) {
                            alarmUserAssetNotification = alarmUserAssetNotificationList.get(0);
                        }
                        if ((alarmAssetNotificationList = alarmAssetNotificationDAO.findByPK(assetId, notificationVO.getUserId())) != null && !alarmAssetNotificationList.isEmpty()) {
                            alarmAssetNotification = alarmAssetNotificationList.get(0);
                        }
                    } catch (IllegalArgumentException e) {
                        throw e;
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                        sendEvent(e.getStackTrace());
                        return "{\"outcome\":\"Error: Failed to find Alarm Notification items to update.\"}";
                    }

                    if (alarmUserAssetNotification != null && alarmAssetNotification != null) {
                        deleteAlarmUserAssetNotificationList.add(alarmUserAssetNotification);
                        deleteAlarmAssetNotificationList.add(alarmAssetNotification);
                    }
                }
            }
            
            //If alarmNotification was at site level, the entries have to be deleted
            if ((alarmUserSiteNotificationList = alarmUserSiteNotificationDAO.findByPK(notificationVO.getUserId(), notificationVO.getSiteId())) != null && !alarmUserSiteNotificationList.isEmpty()){
                deleteAlarmUserSiteNotification = alarmUserSiteNotificationList.get(0);
            }
            if ((alarmSiteNotificationList = alarmSiteNotificationDAO.findByPK(notificationVO.getSiteId(), notificationVO.getUserId())) != null && !alarmSiteNotificationList.isEmpty()){
                deleteAlarmSiteNotification = alarmSiteNotificationList.get(0);
            }
            
            //perform CRUD operation
            try {
                if (!createAlarmUserAssetNotificationList.isEmpty() && !createAlarmAssetNotificationList.isEmpty()) {
                    // Insert updated entities into Cassandra
                    alarmUserAssetNotificationDAO.create(createAlarmUserAssetNotificationList);
                    alarmAssetNotificationDAO.create(createAlarmAssetNotificationList);

                    // Send kafka message
                    createAlarmUserAssetNotificationList.forEach(notification -> {
                        KafkaNotificationVO kafkaNotificationVO = new KafkaNotificationVO();
                        kafkaNotificationVO.setNotificationId(notification.getAssetId());
                        kafkaNotificationVO.setNotificationType("asset_id");
                        kafkaNotificationVO.setEmailAddress(notification.getEmailAddress());
                        kafkaNotificationVO.setAlert(notification.isAlert());
                        kafkaNotificationVO.setFault(notification.isFault());
                        UserService.sendKafkaAlarmNotificationMessage(kafkaNotificationVO);
                    });
                }
                if (!deleteAlarmUserAssetNotificationList.isEmpty() && !deleteAlarmAssetNotificationList.isEmpty()) {
                    // Delete the entities from Cassandra
                    alarmUserAssetNotificationDAO.delete(deleteAlarmUserAssetNotificationList);
                    alarmAssetNotificationDAO.delete(deleteAlarmAssetNotificationList);

                    // send delete kafka message
                    deleteAlarmUserAssetNotificationList.forEach(notification -> {
                        UserService.sendKafkaAlarmNotificationMessage(notification.getAssetId(), "asset_id");
                    });

                }
                
                if (deleteAlarmUserSiteNotification != null)
                    alarmUserSiteNotificationDAO.delete(deleteAlarmUserSiteNotification);
                
                if (deleteAlarmSiteNotification != null) {
                    alarmSiteNotificationDAO.delete(deleteAlarmSiteNotification);
                    // send delete kafka message
                    UserService.sendKafkaAlarmNotificationMessage(deleteAlarmSiteNotification.getSiteId(), "site_id");
                }
                
                return "{\"outcome\":\"Alarm Notification updated successfully.\"}";

            } catch (IllegalArgumentException e) {
                throw e;
            } catch (UnavailableException | WriteTimeoutException e) {
                LOGGER.error(e.getMessage(), e);
                sendEvent(e.getStackTrace());
            }

        }

        return "{\"outcome\":\"Error: Failed to update Alarm Notification items.\"}";

    }

}
