/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.userservice.delegate;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.config.dao.ConfigMapperImpl;
import com.uptime.cassandra.config.dao.SiteCustomerDAO;
import com.uptime.cassandra.config.entity.SiteCustomer;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author kpati
 */
public class ReadSiteCustomerDelegate {
    private final SiteCustomerDAO siteCustomerDAO;

    /**
     * Constructor
     */
    public ReadSiteCustomerDelegate() {
        siteCustomerDAO = ConfigMapperImpl.getInstance().siteCustomerDAO();
    } 
    
    /**
     * Return a List of SiteCustomer Objects by the given params
     * @param customer, String Object
     * @return List of SiteCustomer Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteCustomer> getSiteCustomerByCustomer(String customer) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteCustomerDAO.findByCustomer(customer);
    }
    
    /**
     * Return a List of SiteCustomer Objects by the given params
     * @param customer, String Object
     * @param siteId, UUID Object
     * @return List of SiteCustomer Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<SiteCustomer> getSiteCustomerByPK(String customer, UUID siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return siteCustomerDAO.findByPK(customer, siteId);
    } 
}
