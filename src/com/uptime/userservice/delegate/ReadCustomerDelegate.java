/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.userservice.delegate;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.userpreferences.dao.UserPreferencesMapperImpl;
import com.uptime.cassandra.userpreferences.dao.CustomersDAO;
import com.uptime.cassandra.userpreferences.entity.Customers;
import java.util.List;

/**
 *
 * @author madhavi
 */
public class ReadCustomerDelegate {
    private final CustomersDAO customersDAO;


    /**
     * Constructor
     */
    public ReadCustomerDelegate() {
        customersDAO = UserPreferencesMapperImpl.getInstance().customersDAO();

    }
    
    /**
     * Return a List of Customers Objects by the given params
     * @param customerAccount, String Object
     * 
     * @return List of Customers Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public List<Customers> getCustomersByPK(String customerAccount) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        return customersDAO.findByPK(customerAccount);
    } 
    
    /**
     * Return a List of  all Customers Objects in the customers table
     * 
     * @return List of Customers Objects
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     */
    public List<Customers> getAllCustomers() throws UnavailableException, ReadTimeoutException {
        return customersDAO.findAllCustomers();
    } 
    
}
