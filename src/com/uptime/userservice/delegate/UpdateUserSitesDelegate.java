/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.userservice.delegate;

import com.uptime.cassandra.userpreferences.dao.SiteUsersDAO;
import com.uptime.cassandra.userpreferences.dao.UserPreferencesMapperImpl;
import com.uptime.cassandra.userpreferences.dao.UserSitesDAO;
import com.uptime.cassandra.userpreferences.entity.SiteUsers;
import com.uptime.cassandra.userpreferences.entity.UserSites;
import com.uptime.services.util.ServiceUtil;
import static com.uptime.userservice.UserService.sendEvent;
import com.uptime.userservice.utils.DelegateUtil;
import com.uptime.userservice.vo.SiteUsersVO;
import com.uptime.userservice.vo.UserSitesVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class UpdateUserSitesDelegate {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateUserSitesDelegate.class.getName());
    private final UserSitesDAO userSitesDAO;
    private final SiteUsersDAO siteUsersDAO;

    /**
     * Constructor
     */
    public UpdateUserSitesDelegate() {
        userSitesDAO = UserPreferencesMapperImpl.getInstance().userSitesDAO();
        siteUsersDAO = UserPreferencesMapperImpl.getInstance().siteUsersDAO();
    }

    /**
     * Update Rows in Cassandra based on the given object
     *
     * @param userSitesVO, UserSitesVO object
     * @param siteUsersVO, SiteUsersVO object
     * @return String object
     * @throws IllegalArgumentException
     * @throws Exception
     */
    public String updateUserSites(UserSitesVO userSitesVO, SiteUsersVO siteUsersVO) throws IllegalArgumentException, Exception {
        UserSites userSites;
        SiteUsers siteUsers;
        String errorMsg;
        

        // Insert updated entities into Cassandra
        userSites = DelegateUtil.getUserSites(userSitesVO);
        siteUsers = DelegateUtil.getSiteUsers(siteUsersVO);

        try {
            if ((errorMsg = ServiceUtil.validateObjectData(userSites)) == null && 
                    (errorMsg = ServiceUtil.validateObjectData(siteUsers)) == null) {
                userSitesDAO.update(userSites);
                siteUsersDAO.update(siteUsers);
                return "{\"outcome\":\"Updated User Sites and Site Users items successfully.\"}";
            }
            else
                return errorMsg;
        } catch (IllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            sendEvent(e.getStackTrace());
        }
        return "{\"outcome\":\"Error: Failed to update User Sites and Site Users items.\"}";
    }
}
