/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.userservice.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.userpreferences.entity.PasswordToken;
import com.uptime.userservice.delegate.CreatePasswordTokenDelegate;
import com.uptime.userservice.delegate.ReadPasswordTokenDelegate;
import com.uptime.userservice.utils.JsonUtil;
import com.uptime.userservice.vo.PasswordTokenVO;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author twilcox
 */
public class PasswordTokenController {
    ReadPasswordTokenDelegate readPasswordTokenDelegate;

    /**
     * Constructor
     */
    public PasswordTokenController() {
        readPasswordTokenDelegate = new ReadPasswordTokenDelegate();
    }

    /**
     * Check if the given passwordToken exists
     *
     * @param userId, String Object
     * @param passwordToken, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     */
    public String checkByPK(String userId, String passwordToken) throws UnavailableException, ReadTimeoutException, IllegalArgumentException {
        List<PasswordToken> passwordTokenResult;

        if ((passwordTokenResult = readPasswordTokenDelegate.getPasswordTokenByPK(userId, UUID.fromString(passwordToken))) != null && !passwordTokenResult.isEmpty()) {
            return "{\"found\":true,\"outcome\":\"GET worked successfully.\"}";
        } else {
            return "{\"found\":false,\"outcome\":\"GET worked successfully.\"}";
        }
    }

    /**
     * Create a new PasswordToken by inserting into a table
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String createPasswordToken(String content) throws IllegalArgumentException {
        CreatePasswordTokenDelegate createDelegate;
        PasswordTokenVO passwordTokenVO;

        if ((passwordTokenVO = JsonUtil.passwordTokenParser(content)) != null) {
            if (passwordTokenVO.getUserId() != null && !passwordTokenVO.getUserId().isEmpty()
                    && passwordTokenVO.getPasswordToken() != null) {

                // Insert into Cassandra
                try {
                    createDelegate = new CreatePasswordTokenDelegate();
                    return createDelegate.create(passwordTokenVO);
                } catch (IllegalArgumentException e) {
                    throw e;
                }
                
            } else {
                return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }
}
