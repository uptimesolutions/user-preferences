/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.userservice.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.config.entity.SiteCustomer;
import com.uptime.cassandra.userpreferences.entity.UserPreferences;
import com.uptime.services.util.JsonConverterUtil;
import com.uptime.userservice.delegate.CreateUserPreferencesDelegate;
import com.uptime.userservice.delegate.DeleteUserPreferencesDelegate;
import com.uptime.userservice.delegate.ReadSiteCustomerDelegate;
import com.uptime.userservice.delegate.ReadUserPreferencesDelegate;
import com.uptime.userservice.delegate.UpdateUserPreferencesDelegate;
import com.uptime.userservice.utils.JsonUtil;
import com.uptime.userservice.vo.UserPreferencesVO;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class UserPreferencesController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserPreferencesController.class.getName());
    ReadUserPreferencesDelegate readUserPreferencesDelegate;
    ReadSiteCustomerDelegate readSiteCustomerDelegate;

    /**
     * Constructor
     */
    public UserPreferencesController() {
        readUserPreferencesDelegate = new ReadUserPreferencesDelegate();
        readSiteCustomerDelegate = new ReadSiteCustomerDelegate();
    }

    /**
     * Return a json of a List of UserPreferences Objects by the given params
     * @param userId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String findByPK(String userId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<UserPreferences> userPreferencesResult;
        List<SiteCustomer> siteCustomerResult;
        StringBuilder json;

        if ((userPreferencesResult = readUserPreferencesDelegate.findByPK(userId)) != null) {
            if (!userPreferencesResult.isEmpty()) {
                json = new StringBuilder();
                json
                    .append("{\"userId\":\"").append(userId).append("\",")
                    .append("\"userPreferences\":[");
                for (int i = 0; i < userPreferencesResult.size(); i++) {
                    if (userPreferencesResult.get(i).getCustomerAccount() != null && !userPreferencesResult.get(i).getCustomerAccount().isEmpty() && userPreferencesResult.get(i).getDefaultSite() != null && 
                            (siteCustomerResult = readSiteCustomerDelegate.getSiteCustomerByPK(userPreferencesResult.get(i).getCustomerAccount(), userPreferencesResult.get(i).getDefaultSite())) != null && !siteCustomerResult.isEmpty()) {
                        json.append(JsonConverterUtil.toJSON(userPreferencesResult.get(i), "defaultSiteName", siteCustomerResult.get(0).getSiteName())).append(i < userPreferencesResult.size() - 1 ? "," : "");
                    } else {
                        json.append(JsonConverterUtil.toJSON(userPreferencesResult.get(i))).append(i < userPreferencesResult.size() - 1 ? "," : "");
                    }
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"User Preferences items not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Create a new UserPreferences by inserting into multiple tables
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String createUserPreferences(String content) throws IllegalArgumentException {
        CreateUserPreferencesDelegate createDelegate;
        UserPreferencesVO userPreferencesVO;

        if ((userPreferencesVO = JsonUtil.userPreferencesParser(content)) != null) {
            if (userPreferencesVO != null
                    && userPreferencesVO.getUserId() != null && !userPreferencesVO.getUserId().isEmpty()
                    && userPreferencesVO.getCustomerAccount() != null && !userPreferencesVO.getCustomerAccount().isEmpty()) {

                // Insert into Cassandra
                try {
                    createDelegate = new CreateUserPreferencesDelegate();
                    return createDelegate.createUserPreferences(userPreferencesVO);
                } catch (IllegalArgumentException e) {
                    throw e;
                }
                
            } else {
                return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Update tables dealing with the given UserPreferences
     * @param content, String Object
     * @return String Object
     */
    public String updateUserPreferences(String content) {
        UpdateUserPreferencesDelegate updateDelegate;
        UserPreferencesVO userPreferencesVO;

        if ((userPreferencesVO = JsonUtil.userPreferencesParser(content)) != null) {
            if (userPreferencesVO != null
                    && userPreferencesVO.getUserId() != null && !userPreferencesVO.getUserId().isEmpty()
                    && userPreferencesVO.getCustomerAccount() != null && !userPreferencesVO.getCustomerAccount().isEmpty()) {

                // Update in Cassandra
                try {
                    updateDelegate = new UpdateUserPreferencesDelegate();
                    return updateDelegate.updateUserPreferences(userPreferencesVO);
                } catch (IllegalArgumentException e) {
                    throw e;
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    return "{\"outcome\":\"Error: Failed to update User Preferences items.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Delete tables dealing with the given UserPreferences
     * @param content, String Object
     * @return String Object
     */
    public String deleteUserPreferences(String content) throws IllegalArgumentException {
        DeleteUserPreferencesDelegate deleteDelegate;
        UserPreferencesVO userPreferencesVO;

        if ((userPreferencesVO = JsonUtil.userPreferencesParser(content)) != null) {
            if (userPreferencesVO != null
                    && userPreferencesVO.getUserId() != null && !userPreferencesVO.getUserId().isEmpty()
                    && userPreferencesVO.getCustomerAccount() != null && !userPreferencesVO.getCustomerAccount().isEmpty()) {

                // Delete from Cassandra
                try {
                    deleteDelegate = new DeleteUserPreferencesDelegate();
                    return deleteDelegate.deleteUserPreferences(userPreferencesVO);
                } catch (IllegalArgumentException e) {
                    throw e;
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    return "{\"outcome\":\"Error: Failed to delete User Preferences items.\"}";
                }

            } else {
                return "{\"outcome\":\"Insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }
}
