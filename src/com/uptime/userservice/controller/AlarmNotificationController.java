/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.userservice.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.userpreferences.entity.AlarmUserAssetNotification;
import com.uptime.cassandra.userpreferences.entity.AlarmUserSiteNotification;
import com.uptime.services.util.JsonConverterUtil;
import com.uptime.userservice.delegate.CreateAlarmNotificationDelegate;
import com.uptime.userservice.delegate.DeleteAlarmNotificationDelegate;
import com.uptime.userservice.delegate.ReadAlarmNotificationDelegate;
import com.uptime.userservice.delegate.UpdateAlarmNotificationDelegate;
import com.uptime.userservice.utils.JsonUtil;
import com.uptime.userservice.vo.AlarmNotificationVO;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author aswani
 */
public class AlarmNotificationController {
    private static final Logger LOGGER = LoggerFactory.getLogger(AlarmNotificationController.class.getName());
    ReadAlarmNotificationDelegate delegate;

    /**
     * Constructor
     */
    public AlarmNotificationController() {
        delegate = new ReadAlarmNotificationDelegate();
    }

    /**
     * Return a json of a List of AlarmUserSiteNotification Objects by the given
     * params
     *
     * @param userId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String readAlarmNotificationByUserId(String userId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<AlarmUserSiteNotification> alarmUserSiteNotifications = delegate.getAlarmUserSiteNotificationByUserid(userId);
        StringBuilder json;

        if (alarmUserSiteNotifications != null) {
            if (!alarmUserSiteNotifications.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"userId\":\"").append(userId).append("\",")
                        .append("\"alarmNotifications\":[");
                for (int i = 0; i < alarmUserSiteNotifications.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(alarmUserSiteNotifications.get(i))).append(i < alarmUserSiteNotifications.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Alarm Notifications items not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of a List of AlarmUserAssetNotification Objects or List of
     * AlarmUserSiteNotification Objects by the given params
     *
     * @param userId, String Object
     * @param siteId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String readAlarmNotificationByUserIdSiteId(String userId, String siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<AlarmUserAssetNotification> alarmUserAssetNotifications;
        List<AlarmUserSiteNotification> alarmUserSiteNotifications = null;
        StringBuilder json;

        if (((alarmUserAssetNotifications = delegate.getAlarmUserAssetNotificationByUserIdSiteId(userId, UUID.fromString(siteId))) != null && !alarmUserAssetNotifications.isEmpty() )
                || (alarmUserSiteNotifications = delegate.getAlarmUserSiteNotificationByUseridAssetId(userId, UUID.fromString(siteId))) != null) {

            if (alarmUserAssetNotifications != null && !alarmUserAssetNotifications.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"userId\":\"").append(userId).append("\",")
                        .append("\"alarmNotifications\":[");
                for (int i = 0; i < alarmUserAssetNotifications.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(alarmUserAssetNotifications.get(i))).append(i < alarmUserAssetNotifications.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else if (alarmUserSiteNotifications != null && !alarmUserSiteNotifications.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"userId\":\"").append(userId).append("\",")
                        .append("\"alarmNotifications\":[");
                for (int i = 0; i < alarmUserSiteNotifications.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(alarmUserSiteNotifications.get(i))).append(i < alarmUserSiteNotifications.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Alarm Notifications items not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of a List of AlarmUserAssetNotification Objects by the
     * given params
     *
     * @param userId, String Object
     * @param siteId, String Object
     * @param assetId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String readAlarmNotificationByUserIdSiteIdAssetId(String userId, String siteId, String assetId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<AlarmUserAssetNotification> alarmUserAssetNotifications;
        StringBuilder json;

        if ((alarmUserAssetNotifications = delegate.getAlarmUserAssetNotificationByUserIdSiteIdAssetId(userId, UUID.fromString(siteId), UUID.fromString(assetId))) != null) {
            if (!alarmUserAssetNotifications.isEmpty()) {
                json = new StringBuilder();
                json
                        .append("{\"userId\":\"").append(userId).append("\",")
                        .append("\"alarmNotifications\":[");
                for (int i = 0; i < alarmUserAssetNotifications.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(alarmUserAssetNotifications.get(i))).append(i < alarmUserAssetNotifications.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Alarm Notifications items not found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Insert new rows in the Alarm Notification tables based on the given info
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String createAlarmNotification(String content) throws IllegalArgumentException {
        CreateAlarmNotificationDelegate createDelegate = new CreateAlarmNotificationDelegate();
        AlarmNotificationVO alarmNotificationVO;

        if ((alarmNotificationVO = JsonUtil.alarmNotificationVOsParser(content)) != null) {
            if (alarmNotificationVO == null || alarmNotificationVO.getSiteId() == null
                    || alarmNotificationVO.getUserId() == null || alarmNotificationVO.getUserId().isEmpty()) {
                return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
            }

            // Insert into Cassandra
            try {
                if (createDelegate.createAlarmNotification(alarmNotificationVO).contains("Error: Failed")) {
                    throw new Exception();
                }
                return "{\"outcome\":\"New User Alarm Notification created successfully.\"}";
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                return "{\"outcome\":\"Error: Failed to create all new Alarm Notification.\"}";
            }

        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Insert updated rows in the Alarm Notification tables based on the given
     * info
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String updateAlarmNotification(String content) throws IllegalArgumentException {
        UpdateAlarmNotificationDelegate updateDelegate;
        AlarmNotificationVO alarmNotificationVO;

        if ((alarmNotificationVO = JsonUtil.alarmNotificationVOsParser(content)) != null) {
            if (alarmNotificationVO == null || alarmNotificationVO.getSiteId() == null
                    || alarmNotificationVO.getUserId() == null || alarmNotificationVO.getUserId().isEmpty()) {
                return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
            }

            // Update in Cassandra
            try {
                updateDelegate = new UpdateAlarmNotificationDelegate();
                if (updateDelegate.updateAlarmNotification(alarmNotificationVO).contains("Error: Failed")) {
                    throw new Exception();
                }
                return "{\"outcome\":\"Alarm Notification updated successfully.\"}";
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                return "{\"outcome\":\"Error: Failed to update all Alarm Notification.\"}";
            }

        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Deleted rows in the Alarm Notification tables based on the given info
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String deleteAlarmNotification(String content) throws IllegalArgumentException {
        DeleteAlarmNotificationDelegate deleteDelegate;
        AlarmNotificationVO alarmNotificationVO;

        if ((alarmNotificationVO = JsonUtil.alarmNotificationVOsParser(content)) != null) {
            if (alarmNotificationVO == null || alarmNotificationVO.getSiteId() == null
                    || alarmNotificationVO.getUserId() == null || alarmNotificationVO.getUserId().isEmpty()) {
                return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
            }

            // Delete from Cassandra
            try {
                deleteDelegate = new DeleteAlarmNotificationDelegate();
                if (deleteDelegate.deleteAlarmNotification(alarmNotificationVO).contains("Error: Failed")) {
                    throw new Exception();
                }
                return "{\"outcome\":\"Alarm Notification deleted successfully.\"}";
            } catch (IllegalArgumentException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                return "{\"outcome\":\"Error: Failed to delete all Alarm Notification.\"}";
            }

        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }
    
    /**
     * Deleted rows in the Alarm Notification tables based on the given info
     *
     * @param content, String Object
     * @throws IllegalArgumentException
     */
    public void deleteAlarmNotificationWithUserDeletion(String content) throws IllegalArgumentException {
        DeleteAlarmNotificationDelegate deleteDelegate;
        AlarmNotificationVO alarmNotificationVO;
        
        if ((alarmNotificationVO = JsonUtil.alarmNotificationVOsParser(content)) != null) {
            if (alarmNotificationVO != null && alarmNotificationVO.getSiteId() != null
                    && alarmNotificationVO.getUserId() != null && !alarmNotificationVO.getUserId().isEmpty()) {
                // Delete from Cassandra
                try {
                    deleteDelegate = new DeleteAlarmNotificationDelegate();
                    deleteDelegate.deleteAlarmNotificationWithUserDeletion(alarmNotificationVO);
                } catch (IllegalArgumentException e) {
                    throw e;
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        }
    }

}
