/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.uptime.userservice.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.config.entity.SiteCustomer;
import com.uptime.cassandra.userpreferences.entity.SiteUsers;
import com.uptime.cassandra.userpreferences.entity.UserSites;
import com.uptime.services.util.JsonConverterUtil;
import com.uptime.userservice.delegate.CreateUserSitesDelegate;
import com.uptime.userservice.delegate.DeleteUserSitesDelegate;
import com.uptime.userservice.delegate.ReadSiteCustomerDelegate;
import com.uptime.userservice.delegate.ReadUserSitesDelegate;
import com.uptime.userservice.delegate.UpdateUserSitesDelegate;
import com.uptime.userservice.utils.JsonUtil;
import com.uptime.userservice.vo.UserSitesVO;
import java.util.List;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph
 */
public class UserSitesController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserSitesController.class.getName());
    ReadUserSitesDelegate readUserSitesDelegate;
    ReadSiteCustomerDelegate readSiteCustomerDelegate;

    /**
     * Constructor
     */
    public UserSitesController() {
        readUserSitesDelegate = new ReadUserSitesDelegate();
        readSiteCustomerDelegate = new ReadSiteCustomerDelegate();
    }

    /**
     * Return a json of a List of UserSites Objects by the given params
     *
     * @param userId, String Object
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String findUserSitesByPK(String userId, String customerAccount, String siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<UserSites> userSitesResult;
        List<SiteCustomer> siteCustomerResult;
        StringBuilder json;

        if ((userSitesResult = readUserSitesDelegate.findUserSitesByPK(userId, customerAccount, UUID.fromString(siteId))) != null) {
            if (!userSitesResult.isEmpty()) {
                json = new StringBuilder();
                json
                    .append("{\"userId\":\"").append(userId).append("\",")
                    .append("\"customerAccount\":\"").append(customerAccount).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"userSites\":[");
                for (int i = 0; i < userSitesResult.size(); i++) {
                    if ((siteCustomerResult = readSiteCustomerDelegate.getSiteCustomerByPK(customerAccount, UUID.fromString(siteId))) != null && !siteCustomerResult.isEmpty()) {
                        json.append(JsonConverterUtil.toJSON(userSitesResult.get(i), "siteName", siteCustomerResult.get(0).getSiteName())).append(i < userSitesResult.size() - 1 ? "," : "");
                    } else {
                        json.append(JsonConverterUtil.toJSON(userSitesResult.get(i))).append(i < userSitesResult.size() - 1 ? "," : "");
                    }
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"No User Sites items found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of a List of SiteUsers Objects by the given params
     *
     * @param userId, String Object
     * @param customerAccount, String Object
     * @param siteId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String findSiteUsersByPK(String userId, String customerAccount, String siteId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteUsers> siteUsersResult;
        List<SiteCustomer> siteCustomerResult;
        StringBuilder json;

        if ((siteUsersResult = readUserSitesDelegate.findSiteUsersByPK(userId, customerAccount, UUID.fromString(siteId))) != null) {
            if (!siteUsersResult.isEmpty()) {
                json = new StringBuilder();
                json
                    .append("{\"userId\":\"").append(userId).append("\",")
                    .append("\"customerAccount\":\"").append(customerAccount).append("\",")
                    .append("\"siteId\":\"").append(siteId).append("\",")
                    .append("\"siteUsers\":[");
                for (int i = 0; i < siteUsersResult.size(); i++) {
                    if ((siteCustomerResult = readSiteCustomerDelegate.getSiteCustomerByPK(customerAccount, UUID.fromString(siteId))) != null && !siteCustomerResult.isEmpty()) {
                        json.append(JsonConverterUtil.toJSON(siteUsersResult.get(i), "siteName", siteCustomerResult.get(0).getSiteName())).append(i < siteUsersResult.size() - 1 ? "," : "");
                    } else {
                        json.append(JsonConverterUtil.toJSON(siteUsersResult.get(i))).append(i < siteUsersResult.size() - 1 ? "," : "");
                    }
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"No User Sites items found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of a List of UserSites Objects by the given params
     *
     * @param userId, String Object
     * @param customerAccount, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String findByUserIdcustomerAccount(String userId, String customerAccount) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<UserSites> userSitesResult;
        List<SiteCustomer> siteCustomerResult;
        StringBuilder json;
        boolean notFound;

        if ((userSitesResult = readUserSitesDelegate.findByUserIdcustomerAccount(userId, customerAccount)) != null) {
            if (!userSitesResult.isEmpty()) {
                siteCustomerResult = readSiteCustomerDelegate.getSiteCustomerByCustomer(customerAccount);
                json = new StringBuilder();
                json
                    .append("{\"userId\":\"").append(userId).append("\",")
                    .append("\"customerAccount\":\"").append(customerAccount).append("\",")
                    .append("\"userSites\":[");
                for (int i = 0; i < userSitesResult.size(); i++) {
                    notFound = true;
                    
                    if (siteCustomerResult != null && !siteCustomerResult.isEmpty()) {
                        for (SiteCustomer siteCustomer : siteCustomerResult) {
                            if (siteCustomer.getSiteId().equals(userSitesResult.get(i).getSiteId())) {
                                json.append(JsonConverterUtil.toJSON(userSitesResult.get(i), "siteName", siteCustomer.getSiteName())).append(i < userSitesResult.size() - 1 ? "," : "");
                                notFound = false;
                                break;
                            }
                        }

                    }
                    if (notFound) {
                        json.append(JsonConverterUtil.toJSON(userSitesResult.get(i))).append(i < userSitesResult.size() - 1 ? "," : "");
                    }
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"No User Sites items found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of a List of SiteUsers Objects by the given params
     *
     * @param siteId, String Object
     * @param customerAccount, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String findBySiteIdcustomerAccount(String siteId, String customerAccount) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<SiteUsers> siteUsersResult;
        StringBuilder json;

        if ((siteUsersResult = readUserSitesDelegate.findBysiteIdcustomerAccount(customerAccount, UUID.fromString(siteId))) != null) {
            if (!siteUsersResult.isEmpty()) {
                json = new StringBuilder();
                json
                    .append("{\"siteId\":\"").append(siteId).append("\",")
                    .append("\"customerAccount\":\"").append(customerAccount).append("\",")
                    .append("\"siteUsers\":[");
                for (int i = 0; i < siteUsersResult.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(siteUsersResult.get(i))).append(i < siteUsersResult.size() - 1 ? "," : "");
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"No Sites User items found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a json of a List of UserSites Objects by the given params
     *
     * @param userId, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String findByUserId(String userId) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<UserSites> userSitesResult;
        List<SiteCustomer> siteCustomerResult;
        StringBuilder json;

        if ((userSitesResult = readUserSitesDelegate.findByUserId(userId)) != null) {
            if (!userSitesResult.isEmpty()) {
                json = new StringBuilder();
                json
                    .append("{\"userId\":\"").append(userId).append("\",")
                    .append("\"userSites\":[");
                for (int i = 0; i < userSitesResult.size(); i++) {
                    if ((siteCustomerResult = readSiteCustomerDelegate.getSiteCustomerByPK(userSitesResult.get(i).getCustomerAccount(), userSitesResult.get(i).getSiteId())) != null && !siteCustomerResult.isEmpty()) {
                        json.append(JsonConverterUtil.toJSON(userSitesResult.get(i), "siteName", siteCustomerResult.get(0).getSiteName())).append(i < userSitesResult.size() - 1 ? "," : "");
                    } else {
                        json.append(JsonConverterUtil.toJSON(userSitesResult.get(i))).append(i < userSitesResult.size() - 1 ? "," : "");
                    }
                }
                json.append("],\"outcome\":\"GET worked successfully.\"}");
                return json.toString();
            } else {
                return "{\"outcome\":\"No User Sites items found.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Create a new UserSites by inserting into a table
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String createUserSites(String content) throws IllegalArgumentException {
        CreateUserSitesDelegate createDelegate;
        UserSitesVO userSitesVO;

        if ((userSitesVO = JsonUtil.userSitesParser(content)) != null) {
            if (userSitesVO != null
                    && userSitesVO.getUserId() != null && !userSitesVO.getUserId().isEmpty()
                    && userSitesVO.getCustomerAccount() != null && !userSitesVO.getCustomerAccount().isEmpty()
                    && userSitesVO.getSiteId() != null) {

                // Insert into Cassandra
                try {
                    createDelegate = new CreateUserSitesDelegate();
                    return createDelegate.createUserSites(userSitesVO, JsonUtil.siteUsersParser(content));
                } catch (IllegalArgumentException e) {
                    throw e;
                }

            } else {
                return "{\"outcome\":\"insufficient and/or invalid data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Update tables dealing with the given UserSites
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String updateUserSites(String content) throws IllegalArgumentException {
        UpdateUserSitesDelegate updateDelegate;
        UserSitesVO userSitesVO;

        if ((userSitesVO = JsonUtil.userSitesParser(content)) != null) {
            if (userSitesVO != null
                    && userSitesVO.getUserId() != null && !userSitesVO.getUserId().isEmpty()
                    && userSitesVO.getCustomerAccount() != null && !userSitesVO.getCustomerAccount().isEmpty()
                    && userSitesVO.getSiteId() != null) {

                // Update in Cassandra
                try {
                    updateDelegate = new UpdateUserSitesDelegate();
                    return updateDelegate.updateUserSites(userSitesVO, JsonUtil.siteUsersParser(content));
                } catch (IllegalArgumentException e) {
                    throw e;
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    return "{\"outcome\":\"Error: Failed to update User Sites items.\"}";
                }

            } else {
                return "{\"outcome\":\"insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }

    /**
     * Delete tables dealing with the given UserSites
     *
     * @param content, String Object
     * @return String Object
     * @throws IllegalArgumentException
     */
    public String deleteUserSites(String content) throws IllegalArgumentException {
        DeleteUserSitesDelegate deleteDelegate;
        UserSitesVO userSitesVO;

        if ((userSitesVO = JsonUtil.userSitesParser(content)) != null) {
            if (userSitesVO != null
                    && userSitesVO.getUserId() != null && !userSitesVO.getUserId().isEmpty()
                    && userSitesVO.getCustomerAccount() != null && !userSitesVO.getCustomerAccount().isEmpty()
                    && userSitesVO.getSiteId() != null) {

                // Delete from Cassandra
                try { 
                    // Delete alarm notifications
                    AlarmNotificationController controller = new AlarmNotificationController();
                    controller.deleteAlarmNotificationWithUserDeletion(content);
                    // Delete user
                    deleteDelegate = new DeleteUserSitesDelegate();
                    return deleteDelegate.deleteUserSites(userSitesVO, JsonUtil.siteUsersParser(content));
                } catch (IllegalArgumentException e) {
                    throw e;
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                    return "{\"outcome\":\"Error: Failed to delete User Sites items.\"}";
                }

            } else {
                return "{\"outcome\":\"Insufficient data given in json\"}";
            }
        } else {
            return "{\"outcome\":\"Json is invalid\"}";
        }
    }
}
