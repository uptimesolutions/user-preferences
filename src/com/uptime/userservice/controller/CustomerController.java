/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.userservice.controller;

import com.datastax.oss.driver.api.core.servererrors.ReadTimeoutException;
import com.datastax.oss.driver.api.core.servererrors.UnavailableException;
import com.uptime.cassandra.userpreferences.entity.Customers;
import com.uptime.userservice.delegate.ReadCustomerDelegate;
import com.uptime.services.util.JsonConverterUtil;
import java.util.List;

/**
 *
 * @author madhavi
 */
public class CustomerController {
    ReadCustomerDelegate readDelegate;

    public CustomerController() {
        readDelegate = new ReadCustomerDelegate();
    }

    /**
     * Return a List of Customers Objects by the given params
     *
     * @param customerAccount, String Object
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws IllegalArgumentException if the params are null or empty string
     * @throws java.lang.Exception
     */
    public String getCustomersByPK(String customerAccount) throws UnavailableException, ReadTimeoutException, IllegalArgumentException, Exception {
        List<Customers> result;
        StringBuilder json;

        if ((result = readDelegate.getCustomersByPK(customerAccount.trim())) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                    .append("{")
                    .append("\"customers\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("]}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Error: Customer Data not found in database.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

    /**
     * Return a List of all Customers Objects in the customers table
     *
     * @return String Object
     * @throws UnavailableException if there is not enough replicas alive to
     * achieve the requested consistency level
     * @throws ReadTimeoutException if there's enough replicas alive to process
     * a query with the requested consistency level
     * @throws java.lang.Exception
     */
    public String getAllCustomers() throws UnavailableException, ReadTimeoutException, Exception {
        List<Customers> result;
        StringBuilder json;

        if ((result = readDelegate.getAllCustomers()) != null) {
            if (!result.isEmpty()) {
                json = new StringBuilder();
                json
                    .append("{")
                    .append("\"customers\":[");
                for (int i = 0; i < result.size(); i++) {
                    json.append(JsonConverterUtil.toJSON(result.get(i))).append(i < result.size() - 1 ? "," : "");
                }
                json.append("]}");
                return json.toString();
            } else {
                return "{\"outcome\":\"Error: Customer Data not found in database.\"}";
            }
        } else {
            return "{\"outcome\":\"Error: Null value received from database.\"}";
        }
    }

}
